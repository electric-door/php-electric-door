<?php
require_once('db/database.php');
session_start();

	$display_oc = "";
	$display_lu = "";
	$display_links_oc = "";
	$display_links_lu = "";
	$admin_first_login = "false";
	
	$timestamp = time();
	$datum_time = date("Y-m-d H:i:s", $timestamp);
	
	//check cookies
	if(isset($_COOKIE['user'])) {
		if (isset($_SESSION['id']) AND $_SESSION['id'] == session_id() ) {
		}else{
			$_SESSION['id'] = session_id();
			$_SESSION['roll'] = $_COOKIE['roll'];
			$_SESSION['name'] = $_COOKIE['user'];
			$_SESSION['first_time'] = "false";
			$admin_first_login = $_SESSION['first_time'];
		}
	}

if (isset($_SESSION['id']) AND $_SESSION['id'] == session_id() ) {
	require_once('roll_controll.php');
	$admin_first_login = $_SESSION['first_time'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Electric Door Project</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.datetimepicker.full.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/dataTables.bootstrap4.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/ >
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/jquery.dataTables.min.css"/>
	<link rel="icon" href="img/door.png">
<script>
	var active_alerts = 0;
	var active_alerts_array = [];
	var wait_befor_close = 0

	$(document).ready(function(){
		var logs_old = ""
		var logs_door_old = ""
		var logs_job_old = ""
		var user_door_old = ""
		var select_door_old = ""
		var rollen_door_old = ""
		var roll_controll_door_old = "";
		
		var old_content = "0"
		var build_div_result = ""
		
		var lock_unlock_old = "0"
		var close_open_old = "0"
		var last_command_id = 0
		var new_comm_finish = 0
		var commands_wait = [];
		
		var befehle_logs_door_table;
		var befehle_logs_jobs_table;
		var befehle_logs_table;
		
		var user_user_controll;
		var user_rollen_control;
		
		var first_time_admin_login = <?php echo $admin_first_login ?>
		
		function read_logs() {
		$.ajax({
			url: "read_log.php?randval="+Math.random(),
			success: function (result) {
				date_now = Date.now();
				convert_data = date_now.toString();
				js_timestamp_date = convert_data.substring(0, 10);
				
				if(logs_old != result){
						api_command_log = JSON.parse(result);
						
						for (i in api_command_log.data) {
							
							comm_id = api_command_log.data[i].id;
							comm_time_display = api_command_log.data[i].time_display;
							comm_user = api_command_log.data[i].user;
							comm_command_display = api_command_log.data[i].command_display;
							comm_feedback = api_command_log.data[i].feedback;
							
							//{"id":"'.$row["commad_id"].'","command_time":"'.$row["command_time"].'","execute_time":"'.$row["execute_time"].'","time_display":"'.$command_time_display.'","user":"'.$row["user"].'","command_row":"'.$row["command"].'","command_type":"'.$row["type"].'","command_display":"'.$type_display.' '.$command_display.'","feedback":"'.$feedback_display .' '. $feedback_time_display.'","feedback_row":"'.$row["feedback"].'", "feedback_time":"'.$row["feedback_time"].'"}';
							//now we check commands and build interface notifys
							comm_command_type = api_command_log.data[i].command_type;
							comm_feedback_row = api_command_log.data[i].feedback_row;
							comm_feedback_time = api_command_log.data[i].feedback_time;
							comm_execute_time = api_command_log.data[i].execute_time;
							comm_command_row = api_command_log.data[i].command_row;
							
							command_found = 0
							
							for (o in commands_wait) {
								api_commands = JSON.parse(commands_wait[o]);
								array_id = api_commands.commands[0].id;
								array_type = api_commands.commands[0].type;
								array_command_row = api_commands.commands[0].command_row;
								array_feedback = api_commands.commands[0].feedback;
								array_feedback_time = api_commands.commands[0].feedback_time;
								array_readed = api_commands.commands[0].readed;
								array_execute_time = api_commands.commands[0].execute_time;
								
								if(array_id == comm_id){
									command_found = 1
									if(array_feedback != comm_feedback_row){
										commands_wait[o] = '{"commands":[{"id":"'+array_id+'", "command_row":"'+array_command_row+'", "type":"'+array_type+'", "feedback":"'+comm_feedback_row+'","feedback_time":"'+comm_feedback_time+'", "readed":"'+array_readed+'", "execute_time":"'+array_execute_time+'"}]}'
									}
								}
							}
							
							if(command_found == 0 && comm_feedback_row == "0"){
								commands_wait.push('{"commands":[{"id":"'+comm_id+'", "command_row":"'+comm_command_row+'", "type":"'+comm_command_type+'", "feedback":"'+comm_feedback_row+'", "feedback_time":"'+comm_feedback_time+'", "readed":"0", "execute_time":"'+comm_execute_time+'"}]}');
							}
						}
							
						if(logs_old != ""){
							befehle_logs_table.ajax.reload();
						}else{
							befehle_logs_table = $('#befehle_logs_table').DataTable( {
									"processing": true,
									"serverSide": false,
									"ajax": "read_log.php",
									"columns": [
										{ "data": "id" },
										{ "data": "time_display" },
										{ "data": "user" },
										{ "data": "command_display" },
										{ "data": "feedback" }
									],
									"order": [[ 0, "desc" ]]
							} );
						}
						
						logs_old = result
						}
						
						//check local array with commands 
						for (u in commands_wait) {
								api_commands = JSON.parse(commands_wait[u]);
								array_id = api_commands.commands[0].id;
								array_type = api_commands.commands[0].type;
								array_command_row = api_commands.commands[0].command_row;
								array_feedback = api_commands.commands[0].feedback;
								array_feedback_time = api_commands.commands[0].feedback_time;
								array_readed = api_commands.commands[0].readed;
								array_execute_time = api_commands.commands[0].execute_time;
								
								switch(array_command_row) {
									case "lock":
										command_display = "Zusperren";
										break;
									case "unlock":
											command_display = "Aufsperren";
											break;
									case "lock_forced":
											command_display = "!ZUSPERREN";
											break;
									case "unlock_forced":
											command_display = "!AUFSPERREN";
											break;
									default:
											command_display = "unknow_command";
										}
										
								switch(array_feedback) {
									case "ok_door_unlocked":
										feedback_title = command_display+":";
										feedback_text = "erledigt";
										feedback_alert = "alert-success";
										break;
									case "ok_door_locked":
										feedback_title = command_display+":"
										feedback_text = "erledigt";
										feedback_alert = "alert-success";
										break;
									case "error_already_locked":
										feedback_title = command_display+" Error:";
										feedback_text = "ist bereits zugesperrt";
										feedback_alert = "alert-warning";
										break;
									case "error_already_unlocked":
										feedback_title = command_display+" Error:";
										feedback_text = "ist bereits aufgesperrt";
										feedback_alert = "alert-warning";
										break;
									case "error_door_open":
										feedback_title = command_display+" Error:";
										feedback_text = "Türe steht offen";
										feedback_alert = "alert-danger";
										break;
									default:
										feedback_title = command_display+" Error:";
										feedback_text = "unknow_feedback";
										feedback_alert = "alert-danger";
										}
										
									finish_jobs_now_diff = array_feedback_time - array_execute_time
								
								if(array_feedback != 0){
										if(array_type == "now"){
											//display alert for command done need check if error or ok
											//console.log("Display Finish for Now! " + array_id + "  " + array_feedback)
											
											$("#alert_wait_for_command").remove(); 
											$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert '+feedback_alert+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+feedback_title+'</strong> '+feedback_text+' ('+finish_jobs_now_diff+'s)</div>' + $("#active_alerts").html());
											active_alerts_array.push(active_alerts)
											
											if(array_command_row == "lock" || array_command_row == "lock_forced"){
												$("#lock_button").html('Zusperren');
											}else if (array_command_row == "unlock" || array_command_row == "unlock_forced"){
												$("#unlock_button").html('Aufsperren');
											}
											
											$("#lock_button").prop('disabled', false);
											$("#unlock_button").prop('disabled', false);
										}else{
											//display alert for command done need check if error or ok
											//console.log("Display Finsih for Job! " + array_id + "  " + array_feedback)
											$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert '+feedback_alert+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>A.'+feedback_title+'</strong> '+feedback_text+'</div>' + $("#active_alerts").html());
											active_alerts_array.push(active_alerts)
										}
										
										//remove command from array
										commands_wait.splice(u, 1);
								}else{
									if(array_type == "now"){
										if(array_readed == 0){
											//creat alert warning
											//console.log("Display Wait for NOW! " + array_id + "  " + array_feedback + " " + array_execute_time + " " +js_timestamp_date)
											commands_wait[u] = '{"commands":[{"id":"'+array_id+'", "command_row":"'+array_command_row+'", "type":"'+array_type+'", "feedback":"'+array_feedback+'", "feedback_time":"'+array_feedback_time+'", "readed":"1", "execute_time":"'+array_execute_time+'"}]}';
											
											$("#active_alerts").html('<div id="alert_wait_for_command" class="alert alert-secondary"><strong>'+command_display+':</strong> Warten...</div>' + $("#active_alerts").html());
											
											if(array_command_row == "lock" || array_command_row == "lock_forced"){
												$("#lock_button").html('Zusperren <div class="spinner-border text-danger"></div>');
											}else if (array_command_row == "unlock" || array_command_row == "unlock_forced"){
												$("#unlock_button").html('Aufsperren <div class="spinner-border text-danger"></div>');
											}
											
											$("#lock_button").prop('disabled', true);
											$("#unlock_button").prop('disabled', true);
										}else{
											//update alert warning
											//console.log("Display Update for NOW! " + array_id + "  " + array_feedback)
											
											diff = js_timestamp_date - array_execute_time
											
											$("#alert_wait_for_command").html('<strong>'+command_display+':</strong> Warten... ('+ diff +'s)</div>');
											
											if(array_command_row == "lock" || array_command_row == "lock_forced"){
												$("#lock_button").html('Zusperren <div class="spinner-border text-danger"></div>');
											}else if (array_command_row == "unlock" || array_command_row == "unlock_forced"){
												$("#unlock_button").html('Aufsperren <div class="spinner-border text-danger"></div>');
											}
											
											$("#lock_button").prop('disabled', true);
											$("#unlock_button").prop('disabled', true);
										}
									}else{
										if(array_readed == 0 ){
											commands_wait[u] = '{"commands":[{"id":"'+array_id+'", "command_row":"'+array_command_row+'", "type":"'+array_type+'", "feedback":"'+array_feedback+'", "feedback_time":"'+array_feedback_time+'", "readed":"1", "execute_time":"'+array_execute_time+'"}]}';
											
											if(new_comm_finish == 1){
												//onyl display if command added while we are online
												//console.log("Display Wait for Job! " + array_id + "  " + array_feedback)
												//display alert
												$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Jawoll!</strong> Geplantes aufsperren wurde hinzugefügt.</div>' + $("#active_alerts").html());
												active_alerts_array.push(active_alerts)
											}
										}
									}
								}
						}
						new_comm_finish = 1;
					}
			});
			
		$.ajax({
			url: "read_door_log.php?randval="+Math.random(),
			success: function (result) {
				if(logs_door_old != result){
						if(logs_door_old != ""){
							befehle_logs_door_table.ajax.reload();
						}else{
							befehle_logs_door_table = $('#befehle_logs_door_table').DataTable( {
									"processing": true,
									"serverSide": false,
									"ajax": "read_door_log.php",
									"columns": [
										{ "data": "id" },
										{ "data": "time" },
										{ "data": "command" }
									],
									"order": [[ 0, "desc" ]]
							} );
						}
						logs_door_old = result
					}
			}
		});
		
		$.ajax({
			url: "read_job_commands.php?randval="+Math.random(),
			success: function (result) {
				count_jobs = "";
					if(logs_job_old != result){
						api_job_command = JSON.parse(result);
						
						for (i in api_job_command.data) {
							count_jobs++;
						}
						
						if(count_jobs == 0){
							$('#logs_job_main').hide();
						}else{
							$('#logs_job_main').show();
						}
						
						$('#planen_count').html(count_jobs);
						
						if(logs_job_old != ""){
							befehle_logs_jobs_table.ajax.reload();
						}else{
							befehle_logs_jobs_table = $('#befehle_logs_jobs_table').DataTable( {
									"processing": true,
									"serverSide": false,
									"ajax": "read_job_commands.php",
									"columns": [
										{ "data": "id" },
										{ "data": "time" },
										{ "data": "user" },
										{ "data": "command" },
										{ "data": "zusperren" }
									],
									"order": [[ 0, "desc" ]]
							} );
						}
					logs_job_old = result
				}
			}
		});
		
		$.fn.dataTable.ext.errMode = 'none';
		}
		
		function user_controll() {
			$.ajax({
			url: "read_door_rollen.php?option=select&randval="+Math.random(),
			success: function (result) {
				if(select_door_old != result){
					var select = document.getElementById("new_rolle");
					var select_2 = document.getElementById("mod_rolle");
					
					res = result.split(",");
					res.forEach ((res, index) => {
						select.options[select.options.length] = new Option(res, res);
						select_2.options[select_2.options.length] = new Option(res, res);
					});
					
				select_door_old = result
				}
			}
		});
		
		$.ajax({
			url: "read_door_rollen.php?randval="+Math.random(),
			success: function (result) {
				if(rollen_door_old != result){
						if(rollen_door_old != ""){
							user_rollen_control.ajax.reload();
						}else{
							user_rollen_control = $('#user_rollen_control').DataTable( {
									"processing": true,
									"serverSide": false,
									"ajax": "read_door_rollen.php",
									"paging": false,
									"columns": [
										{ "data": "id" },
										{ "data": "name" },
										{ "data": "daten" },
										{ "data": "used" }
									],
									"order": [[ 0, "asc" ]]
							} );
						}
						rollen_door_old = result
					}
			}
		});
		
		$.ajax({
			url: "read_door_user.php?randval="+Math.random(),
			success: function (result) {
				if(user_door_old != result){
						if(user_door_old != ""){
							user_user_controll.ajax.reload();
						}else{
							user_user_controll = $('#user_user_controll').DataTable( {
									"processing": true,
									"serverSide": false,
									"ajax": "read_door_user.php",
									"paging": false,
									"columns": [
										{ "data": "id" },
										{ "data": "name" },
										{ "data": "rolle" },
										{ "data": "aktiv" }
									],
									"order": [[ 0, "asc" ]]
							} );
							
						}
						user_door_old = result
					}
			}
		});
		}
		
			function check_server() {
			$.ajax({
				url: "read_live_status.php?randval="+Math.random(),
				success: function (result) {
					date_now = Date.now();
					convert_data = date_now.toString();
					date = convert_data.substring(0, 10);
					diff = date - result
					
					if(diff < 20) {
						new_content = 'Server: <font color="green">Online</font>'
						if(old_content != "0" && old_content != new_content){
							//show alert
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>OK! </strong>Server ist wieder online!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
							$("#lock_button").prop('disabled', false);
							$("#unlock_button").prop('disabled', false);
							$("#unlock_button_force").prop('disabled', false);
							$("#lock_button_force").prop('disabled', false);
							
						}
						old_content = new_content
					}else{
						new_content = 'Server: <font color="red">Offline</font>'
						
						if(old_content == "0" || old_content != new_content){
							//show alert
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Server ist Offline!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						}
							
						$("#lock_button").prop('disabled', true);
						$("#unlock_button").prop('disabled', true);
						$("#unlock_button_force").prop('disabled', true);
						$("#lock_button_force").prop('disabled', true);
						
						old_content = new_content
					}
					$('#server_status').html(old_content);
				}
			});
		}
		
		function read_door_status() {
		$.ajax({
			url: "read_door_status.php?randval="+Math.random(),
			success: function (result) {
				if(build_div_result != result){

					api_door_status = JSON.parse(result);
					build_div_result = result
					
					lock_unlock = api_door_status.door_status[0].status
					lu_time = api_door_status.door_status[0].time
					lu_display_time = api_door_status.door_status[0].display_time
					
					close_open = api_door_status.door_status[1].status
					oc_time = api_door_status.door_status[1].time
					oc_display_time = api_door_status.door_status[1].display_time
					
					//debug messages
					//console.log(lock_unlock + " " +lu_time + " "+ lu_display_time);
					//console.log(close_open + " " +oc_time + " "+ oc_display_time);
					if(lock_unlock_old != "0" && lock_unlock_old != lock_unlock){
						if(lock_unlock == "unlocked"){
							// info
							//$("#active_alerts").html('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info! </strong>Türe ist aufgesperrt!</div>' + $("#active_alerts").html());
						}else if(lock_unlock == "locked"){
							//success
							//$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info! </strong>Türe ist zugesperrt!</div>' + $("#active_alerts").html());
						}else{
							//danger
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Ungewöhnlicher Status!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						}
					}
					lock_unlock_old = lock_unlock
					
					if(close_open_old == "0" || close_open_old != close_open){
						if(close_open == "closed"){
							if(close_open_old != "0"){
								//display door opened also by start from page!
								// info
								$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info! </strong>Türe ist geschlossen!</div>' + $("#active_alerts").html());
								active_alerts_array.push(active_alerts)
								//door open, lock+unlock buttom disabled
								$("#lock_button").prop('disabled', false);
								$("#unlock_button").prop('disabled', false);
							}
						}else if(close_open == "opened"){
							//success
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info! </strong>Türe ist geöffnet!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
							
							//door open, lock+unlock buttom disabled
							$("#lock_button").prop('disabled', true);
							$("#unlock_button").prop('disabled', true);
							
						}else{
							//danger
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Ungewöhnlicher Status!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
							//door open, lock+unlock buttom disabled
							$("#lock_button").prop('disabled', true);
							$("#unlock_button").prop('disabled', true);
						}
					}
					close_open_old = close_open
					
					//build new info screen
					
					display_result = "<table width='100%'>";
					display_result = display_result + "<tr>";
					if(close_open == "opened"){
						display_result = display_result +"<td><h4><font color='red'>Türe steht offen!</font><br>"+ oc_display_time +"</h4></td>";
						imageUrl = "real_door_open.png";
					}else{
						display_result = display_result +"<td><h4><font color='green'>Türe ist geschlossen</font><br>"+ oc_display_time +"</h4></td>";
						imageUrl = "real_door_closed.png";
					}
					
					display_result = display_result + "</tr>";
		
					display_result = display_result + "<tr>";
					if(lock_unlock == "unlocked"){
						display_result = display_result +"<td><h4><font color='red'>Türe ist nicht zugesperrt</font><br>"+ lu_display_time +"</h4></td>";
					}else{
						display_result = display_result + "<td><h4><font color='green'>Türe ist zugesperrt!</font><br>"+ lu_display_time +"</h4></td>";
					}
					display_result = display_result + "</tr>";
					display_result = display_result + "</table>";
					
					if(close_open == "opened"){
						$("#lock_button").prop('disabled', true);
						$("#unlock_button").prop('disabled', true);
					}else{
						if(lock_unlock == "unlocked"){
							$("#lock_button").prop('disabled', false);
							$("#unlock_button").prop('disabled', true);
						}else{
							$("#lock_button").prop('disabled', true);
							$("#unlock_button").prop('disabled', false);
						}
					}
					
					$("#door_staus_div").html(display_result);
					$("#door_bg_div").css('background-image', 'url(img/' + imageUrl + ')');
				}
			}
		});
	}
	
	
	function hide_active_alerts() {
		//console.log(active_alerts)
		for (u in active_alerts_array) {
		alert_active = $("#active_alert_"+active_alerts_array[u]).html()
			if(alert_active !== "undefined"){
				//console.log(active_alerts_array[u] + "are_active");
				if(wait_befor_close > 5){
					$("#active_alert_"+active_alerts_array[u]).alert('close')
					active_alerts_array.splice(u, 1);
					wait_befor_close = 0
				}else{
					wait_befor_close++
				}
				break;
			}
		}
	}
	
	function check_roll_changes() {
	$.ajax({
			url: "roll_controll.php?js_check=1&randval="+Math.random(),
			success: function (result) {
				if(roll_controll_door_old != result){
					if(roll_controll_door_old != ""){
						$('#home-tab').tab('show');
						$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Achtung! </strong>Deine Zugriff hat sich geändert<br>Bei Fehlern log dich aus und wieder ein!</div>' + $("#active_alerts").html());
						active_alerts_array.push(active_alerts)
					}
						res = result.split(",");
						res.forEach(check_roll);
						
						function check_roll(roll, index) {
							//console.log(index + ":" + roll);
							if(roll != 1){
								//need disable this featur valid faild
								switch (index) {
								case 0:
									//roll_foce_command
									document.getElementById("force_button_div").style.display = "none";
									break;
								case 1:
									//roll_unlock
									document.getElementById("unlock_button").style.display = "none";
									break;
								case 2:
									//roll_lock
									document.getElementById("lock_button").style.display = "none";
									break;
								case 3:
									//roll_planen
									document.getElementById("panel_plan").style.display = "none";
									break;
								case 4:
									//roll_logs
									document.getElementById("panel_logs").style.display = "none";
									clearInterval(check_interval_logs);
									break;
								case 5:
									//roll_user_controll
									document.getElementById("panel_user").style.display = "none";
									clearInterval(check_interval_user_controll);
									break;
								}
							}else{
								//need disable this featur valid faild
								switch (index) {
								case 0:
									document.getElementById("force_button_div").style.display = "block";
									//roll_foce_command
									break;
								case 1:
									//roll_unlock
									document.getElementById("unlock_button").style.display = "block";
									break;
								case 2:
									//roll_lock
									document.getElementById("lock_button").style.display = "block";
									break;
								case 3:
									//roll_planen
									document.getElementById("panel_plan").style.display = "block";
									break;
								case 4:
									//roll_logs
									document.getElementById("panel_logs").style.display = "block";
									read_logs();
									check_interval_logs = setInterval(read_logs, 1000);
									break;
								case 5:
									//roll_user_controll
									document.getElementById("panel_user").style.display = "block";
									check_interval_user_controll = setInterval(user_controll, 1000);
									user_controll();
									break;
								}
							}
						}
						roll_controll_door_old = result
					}
			}
		});
	}
		//###################run function to cut wait time##############
		var check_interval_logs = setInterval(read_logs, 1000);
		var check_interval_user_controll = setInterval(user_controll, 1000);
		
		
		read_door_status();
		var check_interval_status = setInterval(read_door_status, 1000);
		
		hide_active_alerts();
		var check_hide_alerts = setInterval(hide_active_alerts, 1000);
		
		check_server();
		var check_interval = setInterval(check_server, 3000);
		
		check_roll_changes();
		var check_roll_controll = setInterval(check_roll_changes, 3000);

		//check first time if admin have changed the pw
		if(first_time_admin_login == true){
			$("#active_alerts").html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info! </strong>Bitte ändere das Standardpasswort (Default password)</div>' + $("#active_alerts").html());
			active_alerts_array.push(active_alerts)
		}
		
		
		$('#user_user_controll tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			
			document.getElementById("delet_user").style.display="none";
			document.getElementById("modify_user").style.display="none";
		}
		else {	
			user_user_controll.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
			
			read_data = user_user_controll.row('.selected').data();
			user_aktiv = read_data["aktiv"]
			
			if(user_aktiv != "Ja"){
				document.getElementById("delet_user").style.display="block";
			}else{
				document.getElementById("delet_user").style.display="none";
			}
			document.getElementById("modify_user").style.display="block";
		}
		});
	 
		$('#delet_user').click( function () {
			read_data = user_user_controll.row('.selected').data();
			user_name = read_data["name"];
			user_aktiv = read_data["aktiv"];
			//alert( 'Row index: '+read_data["id"]+ ' '+read_data["name"])
			if(user_aktiv == "Ja"){
				$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Es können nur deaktivierte User gelöscht werden!</div>' + $("#active_alerts").html());
				active_alerts_array.push(active_alerts)
				return;
			}
			
			
			if(user_name == "admin"){
				$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Der User Admin kann nicht gelöscht werden!</div>' + $("#active_alerts").html());
				active_alerts_array.push(active_alerts)
				return;
			}
			
			$.ajax({
			url: "write_detelt_user.php?user_name="+user_name+"&randval="+Math.random(),
			success: function (result) {
				if (result == "OK"){
					$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich! </strong>Benutzer wurde gelöscht!</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
					
					user_user_controll.row('.selected').remove().draw( false );
					document.getElementById("delet_user").style.display="none";
					document.getElementById("modify_user").style.display="none";
				}else{
					$("#active_alerts").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>'+ result +'</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
				}
			}
			});
		});
		
		
		$('#modify_user').click( function () {
			read_data = user_user_controll.row('.selected').data();
			
			user_id = read_data["id"];
			user_name = read_data["name"];
			user_rolle = read_data["rolle"];
			user_aktiv = read_data["aktiv"];
			
			$('#modify_user_modal').modal('show')
			if(user_name == "admin"){
				document.getElementById("mod_rolle").disabled = true;
				document.getElementById("mod_active").disabled = true;
			}else{
				document.getElementById("mod_rolle").disabled = false;
				document.getElementById("mod_active").disabled = false;
			}
			
			document.getElementById("mod_id").value = user_id
			document.getElementById("mod_usr").value = user_name
			document.getElementById("mod_pwd").value = ""
			document.getElementById("mod_pwd_2").value = ""
			document.getElementById("mod_rolle").value = user_rolle
			
			if(user_aktiv == "Ja"){
				document.getElementById("mod_active").checked = true
			}else{
				document.getElementById("mod_active").checked = false
			}
		});
		
		$('#mod_user_button').click( function () {
			id = document.getElementById("mod_id").value
			name = document.getElementById("mod_usr").value
			pw1 = document.getElementById("mod_pwd").value
			pw2 = document.getElementById("mod_pwd_2").value
			rolle = document.getElementById("mod_rolle").value
			aktive = document.getElementById("mod_active").checked
			
			error_mesage = "";
			
			if(pw1 != ""){
				if(pw1.length < 6){
					error_mesage = error_mesage + "<font color='red'><strong>Password muss mehr als 6 Zeichen haben</strong></font><br>";
				}else{
					if(pw1 != pw2)
					{
						error_mesage = error_mesage + "<font color='red'><strong>Passwörter stimmen nicht überrein</strong></font><br>";
					}
				}
			}
			if(error_mesage != ""){
				document.getElementById("mod_user_error").innerHTML = error_mesage;
			}else{
				$.ajax({
				url: "write_mod_user.php?user_id="+id+"&user_name="+name+"&user_password="+pw1+"&user_rolle="+rolle+"&user_aktive="+aktive+"&randval="+Math.random(),
				success: function (result) {
					if (result == "OK"){
						$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich! </strong>Benutzer wurde geändert!</div>' + $("#active_alerts").html());
						active_alerts_array.push(active_alerts)
						$('#modify_user_modal').modal('hide')
						
						document.getElementById("mod_id").value = ""
						document.getElementById("mod_usr").value = ""
						document.getElementById("mod_pwd").value = ""
						document.getElementById("mod_pwd_2").value = ""
						document.getElementById("new_rolle").value = ""
						document.getElementById("new_active").checked = false
						
						document.getElementById("delet_user").style.display="none";
						document.getElementById("modify_user").style.display="none";
					}else{
						$("#active_alerts").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>'+ result +'</div>' + $("#active_alerts").html());
						active_alerts_array.push(active_alerts)
					}
				}
				});
			}
			
		});
		
		
		
		$('#add_user_button').click( function () {
			//alert("add_user");
			name = document.getElementById("new_usr").value
			pw1 = document.getElementById("new_pwd").value
			pw2 = document.getElementById("new_pwd_2").value
			rolle = document.getElementById("new_rolle").value
			aktive = document.getElementById("new_active").checked
			
			error_mesage = "";
			
			//alert(name+" "+ pw1+" "+ pw2+" "+ rolle+" "+ aktive);
			
			//check name
			if(name != ""){
				$.ajax({
				url: "read_user_name.php?user_name="+name+"&randval="+Math.random(),
				success: function (result) {
					if (result == "Error"){
						error_mesage = error_mesage + "<font color='red'><strong>Name bereits vergeben!</strong></font><br>";
						document.getElementById("new_user_error").innerHTML = error_mesage;
					}else{
						if(pw1.length < 6){
							error_mesage = error_mesage + "<font color='red'><strong>Password muss mehr als 6 Zeichen haben</strong></font><br>";
						}else{
							if(pw1 != pw2)
							{
								error_mesage = error_mesage + "<font color='red'><strong>Passwörter stimmen nicht überrein</strong></font><br>";
							}
						}
						if(error_mesage == ""){
						$.ajax({
							url: "write_add_user.php?user_name="+name+"&user_password="+pw1+"&user_rolle="+rolle+"&user_aktive="+aktive+"&randval="+Math.random(),
							success: function (result) {
								if (result == "OK"){
									$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich! </strong>Benutzer wurde angelegt!</div>' + $("#active_alerts").html());
									active_alerts_array.push(active_alerts)
									$('#add_user_modal').modal('hide')
								}else{
									$("#active_alerts").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>'+ result +'</div>' + $("#active_alerts").html());
									active_alerts_array.push(active_alerts)
								}
								
							}
						});
					}
					}
				}
				});
			}else{
				error_mesage = error_mesage + "<font color='red'><strong>Name darf nicht leer sein!</strong></font><br>";
			}
			if(error_mesage != ""){
				document.getElementById("new_user_error").innerHTML = error_mesage;
			}
			
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		$('#user_rollen_control tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			
			document.getElementById("delet_rolle").style.display="none";
			document.getElementById("modify_rolle").style.display="none";
		}
		else {	
			user_rollen_control.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
			
			read_data = user_rollen_control.row('.selected').data();
			user_aktiv = read_data["used"]
			roll_name = read_data["name"]
			
			if(user_aktiv == "Nein"){
				document.getElementById("delet_rolle").style.display="block";
			}else{
				document.getElementById("delet_rolle").style.display="none";
			}
			document.getElementById("modify_rolle").style.display="block";
		}
		});
	 
			$('#delet_rolle').click( function () {
			read_data = user_rollen_control.row('.selected').data();
			roll_name = read_data["name"];
			roll_aktiv = read_data["used"];
			if(roll_aktiv != "Nein"){
				$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Es können nur nicht verwendete Rollen gelöscht werden!</div>' + $("#active_alerts").html());
				active_alerts_array.push(active_alerts)
				return;
			}
			
			
			if(roll_name == "admin"){
				$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>Die Rolle Admin kann nicht gelöscht werden!</div>' + $("#active_alerts").html());
				active_alerts_array.push(active_alerts)
				return;
			}
			
			$.ajax({
			url: "write_detelt_roll.php?roll_name="+roll_name+"&randval="+Math.random(),
			success: function (result) {
				if (result == "OK"){
					$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich! </strong>Rolle wurde gelöscht!</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
					
					user_rollen_control.row('.selected').remove().draw( false );
					document.getElementById("delet_rolle").style.display="none";
					document.getElementById("modify_rolle").style.display="none";
				}else{
					$("#active_alerts").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>'+ result +'</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
				}
			}
			});
		});
		
		$("#new_roll_unlock").on('change', function() {
			if ($(this).val() == '0'){
				hide_week_days("unlock")
			} else if($(this).val() == '1') {
				hide_week_days("unlock")
			}else if($(this).val() == 'daten_time') {
				document.getElementById("new_roll_unlock_daten_time_options").style.display="block";
			}
		});
		
		$("#new_roll_lock").on('change', function() {
			if ($(this).val() == '0'){
				hide_week_days("lock")
			} else if($(this).val() == '1') {
				hide_week_days("lock")
			}else if($(this).val() == 'daten_time') {
				document.getElementById("new_roll_lock_daten_time_options").style.display="block";
			}
		});
		
		$("#new_roll_force").on('change', function() {
			if ($(this).val() == '0'){
				hide_week_days("force")
			} else if($(this).val() == '1') {
				hide_week_days("force")
			}else if($(this).val() == 'daten_time') {
				document.getElementById("new_roll_force_daten_time_options").style.display="block";
			}
		});
		
		$("#new_roll_plan").on('change', function() {
			if ($(this).val() == '0'){
				hide_week_days("plan")
			} else if($(this).val() == '1') {
				hide_week_days("plan")
			}else if($(this).val() == 'daten_time') {
				document.getElementById("new_roll_plan_daten_time_options").style.display="block";
			}
		});
		
		$("#new_roll_logs").on('change', function() {
			if ($(this).val() == '0'){
				hide_week_days("logs")
			} else if($(this).val() == '1') {
				hide_week_days("logs")
			}else if($(this).val() == 'daten_time') {
				document.getElementById("new_roll_logs_daten_time_options").style.display="block";
			}
		});
		
		$("#new_roll_user").on('change', function() {
			if ($(this).val() == '0'){
				hide_week_days("user")
			} else if($(this).val() == '1') {
				hide_week_days("user")
			}else if($(this).val() == 'daten_time') {
				document.getElementById("new_roll_user_daten_time_options").style.display="block";
			}
		});
		
		
			$('#add_roll_button').click( function () {
			//alert("add_user");
			error_mesage = "";
			
			name = document.getElementById("new_roll_name").value
			
			unlock = document.getElementById("new_roll_unlock").value
			lock = document.getElementById("new_roll_lock").value
			force = document.getElementById("new_roll_force").value
			plan = document.getElementById("new_roll_plan").value
			logs = document.getElementById("new_roll_logs").value
			user = document.getElementById("new_roll_user").value
			
			if(unlock == "daten_time"){
				data_array = [];
				data_new = ""
				
				week_mo = document.getElementById("new_roll_unlock_mo").checked
				if(week_mo == true){data_array.push("Mo");}
				week_di = document.getElementById("new_roll_unlock_di").checked
				if(week_di == true){data_array.push("Di");}
				week_mi = document.getElementById("new_roll_unlock_mi").checked
				if(week_mi == true){data_array.push("Mi");}
				week_do = document.getElementById("new_roll_unlock_do").checked
				if(week_do == true){data_array.push("Do");}
				week_fr = document.getElementById("new_roll_unlock_fr").checked
				if(week_fr == true){data_array.push("Fr");}
				week_sa = document.getElementById("new_roll_unlock_sa").checked
				if(week_sa == true){data_array.push("Sa");}
				week_so = document.getElementById("new_roll_unlock_so").checked
				if(week_so == true){data_array.push("So");}
				
				start = document.getElementById("new_roll_unlock_start").value
				end = document.getElementById("new_roll_unlock_end").value
				
				if(start != "00" && end == "00"){
					error_mesage = error_mesage + "<font color='red'><strong>Aufsperren: Wenn Start Uhrzeit gesetzt ist darf End Uhrzeit nicht leer sein.</strong></font><br>";
				}else{
					if(end != "00" && start == "00"){
						error_mesage = error_mesage + "<font color='red'><strong>Aufsperren: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
					}else{
						if(end < start){
							error_mesage = error_mesage + "<font color='red'><strong>Aufsperren: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
						}else{
							if(start == end && start != "00" && end != "00"){
								error_mesage = error_mesage + "<font color='red'><strong>Aufsperren: Start Uhrzeit darf nicht gleich sein wie die End Uhrzeit.</strong></font><br>";
							}else{
								if(start != "00"){
									data_array.push(start);
								}
								if(end != "00"){
									data_array.push(end);
								}
							}
						}
					}
				}
				
				max_array = data_array.length
				i = 0;
				if(max_array == 0){
					error_mesage = error_mesage + "<font color='red'><strong>Aufsperren: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
				}else{
					if(max_array == 2 && start != "" && end != ""){
						error_mesage = error_mesage + "<font color='red'><strong>Aufsperren: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
					}else{
						data_array.forEach ((data, index) => {
							i = i + 1
							if(i == max_array){
								data_new = data_new + "" + data
							}else{
								data_new = data_new + "" + data + "|"
							}
						});
					}
				}
				//set new array to write it to db
				unlock = data_new;
			}
			
			
			if(lock == "daten_time"){
				data_array = [];
				data_new = ""
				
				week_mo = document.getElementById("new_roll_lock_mo").checked
				if(week_mo == true){data_array.push("Mo");}
				week_di = document.getElementById("new_roll_lock_di").checked
				if(week_di == true){data_array.push("Di");}
				week_mi = document.getElementById("new_roll_lock_mi").checked
				if(week_mi == true){data_array.push("Mi");}
				week_do = document.getElementById("new_roll_lock_do").checked
				if(week_do == true){data_array.push("Do");}
				week_fr = document.getElementById("new_roll_lock_fr").checked
				if(week_fr == true){data_array.push("Fr");}
				week_sa = document.getElementById("new_roll_lock_sa").checked
				if(week_sa == true){data_array.push("Sa");}
				week_so = document.getElementById("new_roll_lock_so").checked
				if(week_so == true){data_array.push("So");}
				
				start = document.getElementById("new_roll_lock_start").value
				end = document.getElementById("new_roll_lock_end").value
				
				if(start != "00" && end == "00"){
					error_mesage = error_mesage + "<font color='red'><strong>Zusperren: Wenn Start Uhrzeit gesetzt ist darf End Uhrzeit nicht leer sein.</strong></font><br>";
				}else{
					if(end != "00" && start == "00"){
						error_mesage = error_mesage + "<font color='red'><strong>Zusperren: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
					}else{
						if(end < start){
							error_mesage = error_mesage + "<font color='red'><strong>Zusperren: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
						}else{
							if(start == end && start != "00" && end != "00"){
								error_mesage = error_mesage + "<font color='red'><strong>Zusperren: Start Uhrzeit darf nicht gleich sein wie die End Uhrzeit.</strong></font><br>";
							}else{
								if(start != "00"){
									data_array.push(start);
								}
								if(end != "00"){
									data_array.push(end);
								}
							}
						}
					}
				}
				
				max_array = data_array.length
				i = 0;
				if(max_array == 0){
					error_mesage = error_mesage + "<font color='red'><strong>Zusperren: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
				}else{
					if(max_array == 2 && start != "" && end != ""){
						error_mesage = error_mesage + "<font color='red'><strong>Zusperren: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
					}else{
						data_array.forEach ((data, index) => {
							i = i + 1
							if(i == max_array){
								data_new = data_new + "" + data
							}else{
								data_new = data_new + "" + data + "|"
							}
						});
					}
				}
				//set new array to write it to db
				lock = data_new;
			}
			
			
			if(force == "daten_time"){
				data_array = [];
				data_new = ""
				
				week_mo = document.getElementById("new_roll_force_mo").checked
				if(week_mo == true){data_array.push("Mo");}
				week_di = document.getElementById("new_roll_force_di").checked
				if(week_di == true){data_array.push("Di");}
				week_mi = document.getElementById("new_roll_force_mi").checked
				if(week_mi == true){data_array.push("Mi");}
				week_do = document.getElementById("new_roll_force_do").checked
				if(week_do == true){data_array.push("Do");}
				week_fr = document.getElementById("new_roll_force_fr").checked
				if(week_fr == true){data_array.push("Fr");}
				week_sa = document.getElementById("new_roll_force_sa").checked
				if(week_sa == true){data_array.push("Sa");}
				week_so = document.getElementById("new_roll_force_so").checked
				if(week_so == true){data_array.push("So");}
				
				start = document.getElementById("new_roll_force_start").value
				end = document.getElementById("new_roll_force_end").value
				
				if(start != "00" && end == "00"){
					error_mesage = error_mesage + "<font color='red'><strong>Erzwungene Actionen: Wenn Start Uhrzeit gesetzt ist darf End Uhrzeit nicht leer sein.</strong></font><br>";
				}else{
					if(end != "00" && start == "00"){
						error_mesage = error_mesage + "<font color='red'><strong>Erzwungene Actionen: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
					}else{
						if(end < start){
							error_mesage = error_mesage + "<font color='red'><strong>Erzwungene Actionen: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
						}else{
							if(start == end && start != "00" && end != "00"){
								error_mesage = error_mesage + "<font color='red'><strong>Erzwungene Actionen: Start Uhrzeit darf nicht gleich sein wie die End Uhrzeit.</strong></font><br>";
							}else{
								if(start != "00"){
									data_array.push(start);
								}
								if(end != "00"){
									data_array.push(end);
								}
							}
						}
					}
				}
				
				max_array = data_array.length
				i = 0;
				if(max_array == 0){
					error_mesage = error_mesage + "<font color='red'><strong>Erzwungene Actionen: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
				}else{
					if(max_array == 2 && start != "" && end != ""){
						error_mesage = error_mesage + "<font color='red'><strong>Erzwungene Actionen: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
					}else{
						data_array.forEach ((data, index) => {
							i = i + 1
							if(i == max_array){
								data_new = data_new + "" + data
							}else{
								data_new = data_new + "" + data + "|"
							}
						});
					}
				}
				//set new array to write it to db
				force = data_new;
			}
			
			
			if(plan == "daten_time"){
				data_array = [];
				data_new = ""
				
				week_mo = document.getElementById("new_roll_plan_mo").checked
				if(week_mo == true){data_array.push("Mo");}
				week_di = document.getElementById("new_roll_plan_di").checked
				if(week_di == true){data_array.push("Di");}
				week_mi = document.getElementById("new_roll_plan_mi").checked
				if(week_mi == true){data_array.push("Mi");}
				week_do = document.getElementById("new_roll_plan_do").checked
				if(week_do == true){data_array.push("Do");}
				week_fr = document.getElementById("new_roll_plan_fr").checked
				if(week_fr == true){data_array.push("Fr");}
				week_sa = document.getElementById("new_roll_plan_sa").checked
				if(week_sa == true){data_array.push("Sa");}
				week_so = document.getElementById("new_roll_plan_so").checked
				if(week_so == true){data_array.push("So");}
				
				start = document.getElementById("new_roll_plan_start").value
				end = document.getElementById("new_roll_plan_end").value
				
				if(start != "00" && end == "00"){
					error_mesage = error_mesage + "<font color='red'><strong>Planen: Wenn Start Uhrzeit gesetzt ist darf End Uhrzeit nicht leer sein.</strong></font><br>";
				}else{
					if(end != "00" && start == "00"){
						error_mesage = error_mesage + "<font color='red'><strong>Planen: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
					}else{
						if(end < start){
							error_mesage = error_mesage + "<font color='red'><strong>Planen: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
						}else{
							if(start == end && start != "00" && end != "00"){
								error_mesage = error_mesage + "<font color='red'><strong>Planen: Start Uhrzeit darf nicht gleich sein wie die End Uhrzeit.</strong></font><br>";
							}else{
								if(start != "00"){
									data_array.push(start);
								}
								if(end != "00"){
									data_array.push(end);
								}
							}
						}
					}
				}
				
				max_array = data_array.length
				i = 0;
				if(max_array == 0){
					error_mesage = error_mesage + "<font color='red'><strong>Planen: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
				}else{
					if(max_array == 2 && start != "" && end != ""){
						error_mesage = error_mesage + "<font color='red'><strong>Planen: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
					}else{
						data_array.forEach ((data, index) => {
							i = i + 1
							if(i == max_array){
								data_new = data_new + "" + data
							}else{
								data_new = data_new + "" + data + "|"
							}
						});
					}
				}
				//set new array to write it to db
				plan = data_new;
			}
			
			
			
			if(logs == "daten_time"){
				data_array = [];
				data_new = ""
				
				week_mo = document.getElementById("new_roll_logs_mo").checked
				if(week_mo == true){data_array.push("Mo");}
				week_di = document.getElementById("new_roll_logs_di").checked
				if(week_di == true){data_array.push("Di");}
				week_mi = document.getElementById("new_roll_logs_mi").checked
				if(week_mi == true){data_array.push("Mi");}
				week_do = document.getElementById("new_roll_logs_do").checked
				if(week_do == true){data_array.push("Do");}
				week_fr = document.getElementById("new_roll_logs_fr").checked
				if(week_fr == true){data_array.push("Fr");}
				week_sa = document.getElementById("new_roll_logs_sa").checked
				if(week_sa == true){data_array.push("Sa");}
				week_so = document.getElementById("new_roll_logs_so").checked
				if(week_so == true){data_array.push("So");}
				
				start = document.getElementById("new_roll_logs_start").value
				end = document.getElementById("new_roll_logs_end").value
				
				if(start != "00" && end == "00"){
					error_mesage = error_mesage + "<font color='red'><strong>Logs: Wenn Start Uhrzeit gesetzt ist darf End Uhrzeit nicht leer sein.</strong></font><br>";
				}else{
					if(end != "00" && start == "00"){
						error_mesage = error_mesage + "<font color='red'><strong>Logs: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
					}else{
						if(end < start){
							error_mesage = error_mesage + "<font color='red'><strong>Logs: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
						}else{
							if(start == end && start != "00" && end != "00"){
								error_mesage = error_mesage + "<font color='red'><strong>Logs: Start Uhrzeit darf nicht gleich sein wie die End Uhrzeit.</strong></font><br>";
							}else{
								if(start != "00"){
									data_array.push(start);
								}
								if(end != "00"){
									data_array.push(end);
								}
							}
						}
					}
				}
				
				max_array = data_array.length
				i = 0;
				if(max_array == 0){
					error_mesage = error_mesage + "<font color='red'><strong>Logs: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
				}else{
					if(max_array == 2 && start != "" && end != ""){
						error_mesage = error_mesage + "<font color='red'><strong>Logs: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
					}else{
						data_array.forEach ((data, index) => {
							i = i + 1
							if(i == max_array){
								data_new = data_new + "" + data
							}else{
								data_new = data_new + "" + data + "|"
							}
						});
					}
				}
				//set new array to write it to db
				logs = data_new;
			}
			
			
			
			if(user == "daten_time"){
				data_array = [];
				data_new = ""
				
				week_mo = document.getElementById("new_roll_user_mo").checked
				if(week_mo == true){data_array.push("Mo");}
				week_di = document.getElementById("new_roll_user_di").checked
				if(week_di == true){data_array.push("Di");}
				week_mi = document.getElementById("new_roll_user_mi").checked
				if(week_mi == true){data_array.push("Mi");}
				week_do = document.getElementById("new_roll_user_do").checked
				if(week_do == true){data_array.push("Do");}
				week_fr = document.getElementById("new_roll_user_fr").checked
				if(week_fr == true){data_array.push("Fr");}
				week_sa = document.getElementById("new_roll_user_sa").checked
				if(week_sa == true){data_array.push("Sa");}
				week_so = document.getElementById("new_roll_user_so").checked
				if(week_so == true){data_array.push("So");}
				
				start = document.getElementById("new_roll_user_start").value
				end = document.getElementById("new_roll_user_end").value
				
				if(start != "00" && end == "00"){
					error_mesage = error_mesage + "<font color='red'><strong>User: Wenn Start Uhrzeit gesetzt ist darf End Uhrzeit nicht leer sein.</strong></font><br>";
				}else{
					if(end != "00" && start == "00"){
						error_mesage = error_mesage + "<font color='red'><strong>User: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
					}else{
						if(end < start){
							error_mesage = error_mesage + "<font color='red'><strong>User: End Uhrzeit darf nicht kleiner sein als die Start Uhrzeit.</strong></font><br>";
						}else{
							if(start == end && start != "00" && end != "00"){
								error_mesage = error_mesage + "<font color='red'><strong>User: Start Uhrzeit darf nicht gleich sein wie die End Uhrzeit.</strong></font><br>";
							}else{
								if(start != "00"){
									data_array.push(start);
								}
								if(end != "00"){
									data_array.push(end);
								}
							}
						}
					}
				}
				
				max_array = data_array.length
				i = 0;
				if(max_array == 0){
					error_mesage = error_mesage + "<font color='red'><strong>User: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
				}else{
					if(max_array == 2 && start != "00" && end != "00"){
						error_mesage = error_mesage + "<font color='red'><strong>User: Mindestens 1 Tag muss ausgewählt sein.</strong></font><br>";
					}else{
						data_array.forEach ((data, index) => {
							i = i + 1
							if(i == max_array){
								data_new = data_new + "" + data
							}else{
								data_new = data_new + "" + data + "|"
							}
						});
					}
				}
				//set new array to write it to db
				user = data_new;
			}
			
			
			
			
			
			//check name
			if(name != ""){
				$.ajax({
				url: "read_roll_name.php?roll_name="+name+"&randval="+Math.random(),
				success: function (result) {
					if (result == "Error"){
						error_mesage = error_mesage + "<font color='red'><strong>Name bereits vergeben!</strong></font><br>";
						document.getElementById("new_roll_error").innerHTML = error_mesage;
					}else{
						if(unlock == 0 && lock == 0 && force == 0 && plan == 0 && logs == 0 && user == 0){
							error_mesage = error_mesage + "<font color='red'><strong>Mindestens eine Berechrigung muss aktiviert sein!</strong></font><br>";
						}
						if(error_mesage == ""){
							$.ajax({
							url: "write_add_roll.php?roll_name="+name+"&roll_unlock="+unlock+"&roll_lock="+lock+"&roll_force="+force+"&roll_plan="+plan+"&roll_logs="+logs+"&roll_user="+user+"&randval="+Math.random(),
							success: function (result) {
								if (result == "OK"){
									$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich! </strong>Rolle wurde angelegt!</div>' + $("#active_alerts").html());
									active_alerts_array.push(active_alerts)
									$('#add_roll_modal').modal('hide')
								}else{
									$("#active_alerts").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>'+ result +'</div>' + $("#active_alerts").html());
									active_alerts_array.push(active_alerts)
								}
								
							}
							});
						}
					}
				}
				});
			}else{
				error_mesage = error_mesage + "<font color='red'><strong>Name darf nicht leer sein!</strong></font><br>";
			}
			
			if(error_mesage != ""){
				document.getElementById("new_roll_error").innerHTML = error_mesage;
			}
		});
		
		function hide_week_days(priv_id){
			switch (priv_id) {
				case "unlock":
				document.getElementById("new_roll_unlock_daten_time_options").style.display="none";
				document.getElementById("new_roll_unlock_mo").checked = false
				document.getElementById("new_roll_unlock_di").checked = false
				document.getElementById("new_roll_unlock_mi").checked = false
				document.getElementById("new_roll_unlock_do").checked = false
				document.getElementById("new_roll_unlock_fr").checked = false
				document.getElementById("new_roll_unlock_sa").checked = false
				document.getElementById("new_roll_unlock_so").checked = false
				document.getElementById("new_roll_unlock_start").value = ""
				document.getElementById("new_roll_unlock_end").value = ""
				break;
				case "lock":
				document.getElementById("new_roll_lock_daten_time_options").style.display="none";
				document.getElementById("new_roll_lock_mo").checked = false
				document.getElementById("new_roll_lock_di").checked = false
				document.getElementById("new_roll_lock_mi").checked = false
				document.getElementById("new_roll_lock_do").checked = false
				document.getElementById("new_roll_lock_fr").checked = false
				document.getElementById("new_roll_lock_sa").checked = false
				document.getElementById("new_roll_lock_so").checked = false
				document.getElementById("new_roll_lock_start").value = ""
				document.getElementById("new_roll_lock_end").value = ""
				break;
				case "force":
				document.getElementById("new_roll_force_daten_time_options").style.display="none";
				document.getElementById("new_roll_force_mo").checked = false
				document.getElementById("new_roll_force_di").checked = false
				document.getElementById("new_roll_force_mi").checked = false
				document.getElementById("new_roll_force_do").checked = false
				document.getElementById("new_roll_force_fr").checked = false
				document.getElementById("new_roll_force_sa").checked = false
				document.getElementById("new_roll_force_so").checked = false
				document.getElementById("new_roll_force_start").value = ""
				document.getElementById("new_roll_force_end").value = ""
				break;
				case "plan":
				document.getElementById("new_roll_plan_daten_time_options").style.display="none";
				document.getElementById("new_roll_plan_mo").checked = false
				document.getElementById("new_roll_plan_di").checked = false
				document.getElementById("new_roll_plan_mi").checked = false
				document.getElementById("new_roll_plan_do").checked = false
				document.getElementById("new_roll_plan_fr").checked = false
				document.getElementById("new_roll_plan_sa").checked = false
				document.getElementById("new_roll_plan_so").checked = false
				document.getElementById("new_roll_plan_start").value = ""
				document.getElementById("new_roll_plan_end").value = ""
				break;
				case "logs":
				document.getElementById("new_roll_logs_daten_time_options").style.display="none";
				document.getElementById("new_roll_logs_mo").checked = false
				document.getElementById("new_roll_logs_di").checked = false
				document.getElementById("new_roll_logs_mi").checked = false
				document.getElementById("new_roll_logs_do").checked = false
				document.getElementById("new_roll_logs_fr").checked = false
				document.getElementById("new_roll_logs_sa").checked = false
				document.getElementById("new_roll_logs_so").checked = false
				document.getElementById("new_roll_logs_start").value = ""
				document.getElementById("new_roll_logs_end").value = ""
				break;
				case "user":
				document.getElementById("new_roll_user_daten_time_options").style.display="none";
				document.getElementById("new_roll_user_mo").checked = false
				document.getElementById("new_roll_user_di").checked = false
				document.getElementById("new_roll_user_mi").checked = false
				document.getElementById("new_roll_user_do").checked = false
				document.getElementById("new_roll_user_fr").checked = false
				document.getElementById("new_roll_user_sa").checked = false
				document.getElementById("new_roll_user_so").checked = false
				document.getElementById("new_roll_user_start").value = ""
				document.getElementById("new_roll_user_end").value = ""
				break;
				
			}
			
		}
		
		$('#add_roll_modal').on('hidden.bs.modal', function () {
			document.getElementById("new_roll_name").value = ""
			document.getElementById("new_roll_unlock").value = ""
			document.getElementById("new_roll_lock").value = ""
			document.getElementById("new_roll_force").value = ""
			document.getElementById("new_roll_plan").value = ""
			document.getElementById("new_roll_logs").value = ""
			document.getElementById("new_roll_user").value = ""

			hide_week_days("unlock")
			hide_week_days("lock")
			hide_week_days("force")
			hide_week_days("plan")
			hide_week_days("logs")
			hide_week_days("user")

			document.getElementById("delet_user").style.display="none";
			document.getElementById("modify_user").style.display="none";
		});
		
		$('#add_user_modal').on('hidden.bs.modal', function () {
			document.getElementById("new_usr").value = ""
			document.getElementById("new_pwd").value = ""
			document.getElementById("new_pwd_2").value = ""
			document.getElementById("new_rolle").value = ""
			document.getElementById("new_active").checked = false

			document.getElementById("delet_user").style.display="none";
			document.getElementById("modify_user").style.display="none";
		});
		
		$('#modify_rolle').click( function () {
			read_data = user_rollen_control.row('.selected').data();
			
			roll_id = read_data["id"];
			roll_name = read_data["name"];
			
			if(roll_name == "admin")
			{
				$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info! </strong>Die Rolle admin kannst du nicht bearbeiten oder löschen!</div>' + $("#active_alerts").html());
				active_alerts_array.push(active_alerts)
			}else{
				$('#modify_roll_modal').modal('show')
			
				document.getElementById("mod_roll_id").value = roll_id
				document.getElementById("mod_roll_usr").value = roll_name
			}
		});
		
		//#############################################################
});
  </script>
  <style>
.container {
	margin-top: 10px;
}
#active_alerts {
	margin-top: 10px;
	margin-bottom: 10px;
}

#lock_button {
	margin-top: 10px;
}

#unlock_button {
	margin-top: 10px;
}

#fore_buttons {
	margin-top: 10px;
	margin-bottom: 10px;
}
#fore_buttons_active {
	margin-top: 40px;
	margin-bottom:20px;
}

#door_staus_div {
	margin-top: 10px;
}

#door_bg_div {
	height: 210px;
	width: 100%;
	background-repeat: no-repeat;
	background-position: center center;
	background-image: url("img/real_door_closed.png");
}


</style>
</head>
<body>


<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="navbar-brand" href="#">door.nirus.at</a>

  <!-- Links -->
  <ul class="navbar-nav">
	  <li class="nav-item">
		  <a class="nav-link disabled" href="#"><div id="server_status"></div></a>
	</li>
  </ul>
</nav>

<div class="container">
	<?php
	
	
	if (isset($_SESSION['id']) AND $_SESSION['id'] == session_id() ) {
	 ?>

	<div align="right"><h3>Hallo <div id="user_login" style="display:inline-block;"><?php echo $_SESSION['name']; ?></div> <a href="logout.php" class="btn btn-warning btn-sm" role="button">[Logout]</a></h3></div>

		<!-- Nav pills -->
	<ul class="nav nav-pills">
	  <li class="nav-item">
		<a class="nav-link active" id="home-tab" data-toggle="pill" href="#home">Steuern</a>
	  </li>
	  <li id="panel_plan" class="nav-item" <?php if ($roll_planen){ echo 'style="display:block;"'; }?>>
		<a class="nav-link" data-toggle="pill" href="#menu1">Planen <span id="planen_count" class="badge badge-dark">1</span></a>
	  </li>
	  
	  <li id="panel_logs" class="nav-item" <?php if ($roll_logs){ echo 'style="display:block;"'; }?>>
		<a class="nav-link" data-toggle="pill" href="#menu2">Logs</a>
	  </li>
	  
	  <li id="panel_user" class="nav-item" <?php if ($roll_user_controll){ echo 'style="display:block;"'; }?>>
		<a class="nav-link" data-toggle="pill" href="#menu3">User</a>
	  </li>

	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		
		<div id="active_alerts"></div>
		
		<div class="tab-pane container active" id="home">
			<div class="row">
				<div class="col-12"><div id="door_bg_div"></div></div>
			</div>
			
			<div class="row">
				<div class="col-12 d-flex justify-content-center"><div id="door_staus_div"></div></div>
			</div>
	
			<div class="row">
				<div class="col-12"><button <?php if ($roll_lock){ echo 'style="display:block;"'; }?> id="lock_button" type="button" class="btn btn-primary btn-lg btn-block" onclick="click_send_command('lock');">Zusperren</button></div>
			</div>

			<div class="row">
				<div class="col-12"><button <?php if ($roll_unlock){ echo 'style="display:block;"'; }?> id="unlock_button" type="button" class="btn btn-warning btn-lg btn-block" onclick="click_send_command('unlock');">Aufsperren</button></div>
			</div>
			
			<div id="force_button_div" class="row" <?php if ($roll_foce_command){ echo 'style="display:block;"'; }?>>
				<div class="col-12">
					<a href="#fore_buttons" id="fore_buttons_active" class="btn btn-dark btn-sm btn-block" data-toggle="collapse">Mehr...</a>
					<div id="fore_buttons" class="collapse">
						<button id="lock_button_force" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ask_confirm_locked">Zusperren erzwingen!</button>
						<button id="unlock_button_force" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ask_confirm_unlocked">Aufsperren erzwingen!</button>
					</div>
				</div>
			</div>

		</div>
		<div class="tab-pane container fade" id="menu1">
			<div class="row">
				<div id="logs_job_main" class="col-12">
						<table id="befehle_logs_jobs_table" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>ID</th>
									<th>Time</th>
									<th>User</th>
									<th>Befehl</th>
									<th>A.Zusperren</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Time</th>
									<th>User</th>
									<th>Befehl</th>
									<th>A.Zusperren</th>
								</tr>
							</tfoot>
						</table>
				</div>
			</div>
			</br>
			<div class="row">
				<div class="col-12 d-flex justify-content-center"><p class="font-weight-bold">Datum zum AUTOMATISCHEN AUFSPERREN wählen:</p></div>
			</div>
			<div class="row">
				<div class="col-12 d-flex justify-content-center"><input name="datetimepicker" id="datetimepicker" type="text" value=""></div>
				
			</div>
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<div class="form-check">
					  <label class="form-check-label">
						<input type="checkbox" class="form-check-input" id="lock_after_close" value="lock_after_close"><p class="font-weight-bold">Tür zusperren sobald geschlossen?</p>
					  </label>
					</div>
				</div>
			</div>
			
			<button id="save_planed" type="button" class="btn btn-primary btn-lg btn-block" onclick="save_plan()">Speichern</button>
		</div>
	
		<div class="tab-pane container fade" id="menu2">
		<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			  <li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#befehle_logs">Befehle</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#status_logs">Status</a>
			  </li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane container active" id="befehle_logs">
				<div class="row">
					<div class="col-12">
						<table id="befehle_logs_table" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>ID</th>
									<th>Time</th>
									<th>User</th>
									<th>Befehle</th>
									<th>Feedback</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Time</th>
									<th>User</th>
									<th>Befehle</th>
									<th>Feedback</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			  
			  </div>
			  <div class="tab-pane container fade" id="status_logs">
				<div class="row">
					<div class="col-12">
						<table id="befehle_logs_door_table" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>ID</th>
								<th>Time</th>
								<th>Tür Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Time</th>
								<th>Tür Status</th>
							</tr>
						</tfoot>
						</table>
					</div>
				</div>
			  </div>
			</div>
		</div>
		
		<div class="tab-pane container fade" id="menu3">
		<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			  <li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#user_control">Benutzer</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#rollen_control">Rollen</a>
			  </li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane container active" id="user_control">
				<div class="row">
					<div class="col-6">
						<button id="add_user" type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#add_user_modal">Hinzufügen</button>
					</div>
					<div class="col-12">
						<table id="user_user_controll" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Rolle</th>
								<th>Aktiv</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Rolle</th>
								<th>Aktiv</th>
							</tr>
						</tfoot>
						</table>
					</div>
					<div class="col-6">
						<button id="delet_user" type="button" class="btn btn-danger btn-lg btn-block" style="display:none;">Löschen</button>
						<button id="modify_user" type="button" class="btn btn-primary btn-lg btn-block" style="display:none;">Bearbeiten</button>
					</div>
				</div>
			  
			  </div>
			  <div class="tab-pane container fade" id="rollen_control">
				<div class="row">
				<div class="col-6">
						<button id="add_rolle" type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#add_roll_modal">Hinzufügen</button>
					</div>
					<div class="col-12">
						<table id="user_rollen_control" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Daten</th>
								<th>Used</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Daten</th>
								<th>Used</th>
							</tr>
						</tfoot>
						</table>
					</div>
					<div class="col-6">
						<button id="delet_rolle" type="button" class="btn btn-danger btn-lg btn-block" style="display:none;">Löschen</button>
						<button id="modify_rolle" type="button" class="btn btn-primary btn-lg btn-block" style="display:none;">Bearbeiten</button>
					</div>
				</div>
			  </div>
			</div>
		</div>
		
		
		
		
	</div>
	
<!-- Modal -->
<div class="modal fade" id="ask_confirm_locked" tabindex="-1" role="dialog" aria-labelledby="ask_confirm_lockedTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ask_confirm_lockedLongTitle">Zusperren erzwingen?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">Zusperren wird erzwungen egal in welchen Status die Türe sich gerade befindet, gilt auch wenn die Türe offen steht!</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
        <button type="button" class="btn btn-primary" onclick="click_send_command('lock_forced');">JA!</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ask_confirm_unlocked" tabindex="-1" role="dialog" aria-labelledby="ask_confirm_unlockedTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ask_confirm_unlockedLongTitle">Aufsperren erzwingen?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">Aufsperren wird erzwungen egal in welchen Status die Türe sich gerade befindet, gilt auch wenn die Türe offen steht!</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
        <button type="button" class="btn btn-primary" onclick="click_send_command('unlock_forced');">JA!</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_user_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="add_user_modalTitle">Neuen User hinzufügen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div id="new_user_error"></div>
			<div class="form-group">
				<label for="new_usr">Name:</label>
				<input type="text" class="form-control" id="new_usr" name="username">
			</div>
			<div class="form-group">
				<label for="new_pwd">Password:</label>
				<input type="password" class="form-control" id="new_pwd" name="password">
			</div>
			<div class="form-group">
				<label for="new_pwd_2">Password wiederholen:</label>
				<input type="password" class="form-control" id="new_pwd_2" name="password">
			</div>
			<div class="form-group">
			<label for="new_rolle">Rolle</label>
			<select id="new_rolle" class="form-control"></select>
			</div>
			<div class="form-group">
			<div class="form-check">
			<input class="form-check-input" type="checkbox" id="new_active">
			<label class="form-check-label" for="new_active">
				Soll dieser User gleich aktiv sein?
			</label>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
        <button id="add_user_button" type="button" class="btn btn-primary">Speichern</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modify_user_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modify_user_modalTitle">User bearbeiten</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div id="mod_user_error"></div>
			<div class="form-group">
				<label for="mod_id">ID:</label>
				<input type="text" class="form-control" id="mod_id" name="ID" disabled>
			</div>
			<div class="form-group">
				<label for="mod_usr">Name:</label>
				<input type="text" class="form-control" id="mod_usr" name="username" disabled>
			</div>
			<div class="form-group">
				<label for="mod_pwd">Password:</label>
				<input type="password" class="form-control" id="mod_pwd" name="password">
			</div>
			<div class="form-group">
				<label for="mod_pwd_2">Password wiederholen:</label>
				<input type="password" class="form-control" id="mod_pwd_2" name="password">
			</div>
			<div class="form-group">
			<label for="mod_rolle">Rolle</label>
			<select id="mod_rolle" class="form-control"></select>
			</div>
			<div class="form-group">
			<div class="form-check">
			<input class="form-check-input" type="checkbox" id="mod_active">
			<label class="form-check-label" for="mod_active">
				User ist aktiv
			</label>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
        <button id="mod_user_button" type="button" class="btn btn-primary">Speichern</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modify_roll_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modify_user_modalTitle">Rolle bearbeiten</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
			<font color="red">Noch nicht verfügbar, bitte lösche die Rolle und erstelle eine neue.</font>
			
			<div id="mod_roll_error"></div>
			<div class="form-group">
				<label for="mod_id">ID:</label>
				<input type="text" class="form-control" id="mod_roll_id" name="ID" disabled>
			</div>
			<div class="form-group">
				<label for="mod_usr">Name:</label>
				<input type="text" class="form-control" id="mod_roll_usr" name="roll_name" disabled>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
        <button id="mod_roll_button" type="button" class="btn btn-primary" disabled >Speichern</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_roll_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="add_roll_modalTitle">Neuen Rolle hinzufügen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div id="new_roll_error"></div>
			<div class="form-group">
				<label for="new_roll_name">Name:</label>
				<input type="text" class="form-control" id="new_roll_name" name="username">
			</div>
			<div class="form-group">
				<label for="new_roll_unlock">Aufsperren:</label>
				<select id="new_roll_unlock" class="form-control">
					<option value="0">Nein</option>
					<option value="1">Ja</option>
					<option value="daten_time">Ja - Zeitplan</option>
				</select>
			</div>
			<div id="new_roll_unlock_daten_time_options" style="display:none;" class="border border-primary">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_mo" value="mo">
					<label class="form-check-label" for="new_roll_unlock_mo">Montag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_di" value="di">
					<label class="form-check-label" for="new_roll_unlock_di">Dienstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_mi" value="mi">
					<label class="form-check-label" for="new_roll_unlock_mi">Mittwoch</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_do" value="do">
					<label class="form-check-label" for="new_roll_unlock_do">Donnerstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_fr" value="fr">
					<label class="form-check-label" for="new_roll_unlock_fr">Freitag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_sa" value="sa">
					<label class="form-check-label" for="new_roll_unlock_sa">Samstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_unlock_so" value="so">
					<label class="form-check-label" for="new_roll_unlock_so">Sonntag</label>
				</div>
				<div class="form-group">
					<label for="new_roll_unlock_start">Von:</label>
					<select id="new_roll_unlock_start" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
				</div>
				<div class="form-group">
					<label for="new_roll_unlock_end">Bis:</label>
					<select id="new_roll_unlock_end" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
			</div>
		</div>
			<div class="form-group">
				<label for="new_roll_lock">Zusperren:</label>
				<select id="new_roll_lock" class="form-control">
					<option value="0">Nein</option>
					<option value="1">Ja</option>
					<option value="daten_time">Ja - Zeitplan</option>
				</select>
			</div>
			<div id="new_roll_lock_daten_time_options" style="display:none;" class="border border-primary">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_mo" value="mo">
					<label class="form-check-label" for="new_roll_lock_mo">Montag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_di" value="di">
					<label class="form-check-label" for="new_roll_lock_di">Dienstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_mi" value="mi">
					<label class="form-check-label" for="new_roll_lock_mi">Mittwoch</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_do" value="do">
					<label class="form-check-label" for="new_roll_lock_do">Donnerstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_fr" value="fr">
					<label class="form-check-label" for="new_roll_lock_fr">Freitag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_sa" value="sa">
					<label class="form-check-label" for="new_roll_lock_sa">Samstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_lock_so" value="so">
					<label class="form-check-label" for="new_roll_lock_so">Sonntag</label>
				</div>
				<div class="form-group">
					<label for="new_roll_lock_start">Von:</label>
					<select id="new_roll_lock_start" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
				</div>
				<div class="form-group">
					<label for="new_roll_lock_end">Bis:</label>
					<select id="new_roll_lock_end" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
			</div>
		</div>
			<div class="form-group">
				<label for="new_roll_force">Erzwungene Actionen:</label>
				<select id="new_roll_force" class="form-control">
					<option value="0">Nein</option>
					<option value="1">Ja</option>
					<option value="daten_time">Ja - Zeitplan</option>
				</select>
			</div>
			<div id="new_roll_force_daten_time_options" style="display:none;" class="border border-primary">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_mo" value="mo">
					<label class="form-check-label" for="new_roll_force_mo">Montag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_di" value="di">
					<label class="form-check-label" for="new_roll_force_di">Dienstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_mi" value="mi">
					<label class="form-check-label" for="new_roll_force_mi">Mittwoch</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_do" value="do">
					<label class="form-check-label" for="new_roll_force_do">Donnerstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_fr" value="fr">
					<label class="form-check-label" for="new_roll_force_fr">Freitag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_sa" value="sa">
					<label class="form-check-label" for="new_roll_force_sa">Samstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_force_so" value="so">
					<label class="form-check-label" for="new_roll_force_so">Sonntag</label>
				</div>
				<div class="form-group">
					<label for="new_roll_force_start">Von:</label>
					<select id="new_roll_force_start" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
				</div>
				<div class="form-group">
					<label for="new_roll_force_end">Bis:</label>
					<select id="new_roll_force_end" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
			</div>
		</div>
		<div class="form-group">
				<label for="new_roll_plan">Planen:</label>
				<select id="new_roll_plan" class="form-control">
					<option value="0">Nein</option>
					<option value="1">Ja</option>
					<option value="daten_time">Ja - Zeitplan</option>
				</select>
			</div>
			<div id="new_roll_plan_daten_time_options" style="display:none;" class="border border-primary">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_mo" value="mo">
					<label class="form-check-label" for="new_roll_plan_mo">Montag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_di" value="di">
					<label class="form-check-label" for="new_roll_plan_di">Dienstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_mi" value="mi">
					<label class="form-check-label" for="new_roll_plan_mi">Mittwoch</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_do" value="do">
					<label class="form-check-label" for="new_roll_plan_do">Donnerstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_fr" value="fr">
					<label class="form-check-label" for="new_roll_plan_fr">Freitag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_sa" value="sa">
					<label class="form-check-label" for="new_roll_plan_sa">Samstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_plan_so" value="so">
					<label class="form-check-label" for="new_roll_plan_so">Sonntag</label>
				</div>
				<div class="form-group">
					<label for="new_roll_plan_start">Von:</label>
					<select id="new_roll_plan_start" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
				</div>
				<div class="form-group">
					<label for="new_roll_plan_end">Bis:</label>
					<select id="new_roll_plan_end" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
			</div>
		</div>
		<div class="form-group">
				<label for="new_roll_logs">Logs:</label>
				<select id="new_roll_logs" class="form-control">
					<option value="0">Nein</option>
					<option value="1">Ja</option>
					<option value="daten_time">Ja - Zeitplan</option>
				</select>
			</div>
			<div id="new_roll_logs_daten_time_options" style="display:none;" class="border border-primary">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_mo" value="mo">
					<label class="form-check-label" for="new_roll_logs_mo">Montag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_di" value="di">
					<label class="form-check-label" for="new_roll_logs_di">Dienstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_mi" value="mi">
					<label class="form-check-label" for="new_roll_logs_mi">Mittwoch</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_do" value="do">
					<label class="form-check-label" for="new_roll_logs_do">Donnerstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_fr" value="fr">
					<label class="form-check-label" for="new_roll_logs_fr">Freitag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_sa" value="sa">
					<label class="form-check-label" for="new_roll_logs_sa">Samstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_logs_so" value="so">
					<label class="form-check-label" for="new_roll_logs_so">Sonntag</label>
				</div>
				<div class="form-group">
					<label for="new_roll_logs_start">Von:</label>
					<select id="new_roll_logs_start" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
				</div>
				<div class="form-group">
					<label for="new_roll_logs_end">Bis:</label>
					<select id="new_roll_logs_end" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
			</div>
		</div>
		<div class="form-group">
				<label for="new_roll_user">User:</label>
				<select id="new_roll_user" class="form-control">
					<option value="0">Nein</option>
					<option value="1">Ja</option>
					<option value="daten_time">Ja - Zeitplan</option>
				</select>
			</div>
			<div id="new_roll_user_daten_time_options" style="display:none;" class="border border-primary">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_mo" value="mo">
					<label class="form-check-label" for="new_roll_user_mo">Montag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_di" value="di">
					<label class="form-check-label" for="new_roll_user_di">Dienstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_mi" value="mi">
					<label class="form-check-label" for="new_roll_user_mi">Mittwoch</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_do" value="do">
					<label class="form-check-label" for="new_roll_user_do">Donnerstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_fr" value="fr">
					<label class="form-check-label" for="new_roll_user_fr">Freitag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_sa" value="sa">
					<label class="form-check-label" for="new_roll_user_sa">Samstag</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" id="new_roll_user_so" value="so">
					<label class="form-check-label" for="new_roll_user_so">Sonntag</label>
				</div>
				<div class="form-group">
					<label for="new_roll_user_start">Von:</label>
					<select id="new_roll_user_start" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
				</div>
				<div class="form-group">
					<label for="new_roll_user_end">Bis:</label>
					<select id="new_roll_user_end" class="form-control">
						<option value="00">--</option>
						<option value="01">01:00</option>
						<option value="02">02:00</option>
						<option value="03">03:00</option>
						<option value="04">04:00</option>
						<option value="05">05:00</option>
						<option value="06">06:00</option>
						<option value="07">07:00</option>
						<option value="08">08:00</option>
						<option value="09">09:00</option>
						<option value="10">10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
						<option value="23">23:00</option>
						<option value="24">24:00</option>
					</select>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
        <button id="add_roll_button" type="button" class="btn btn-primary">Speichern</button>
      </div>
    </div>
  </div>
</div>
	
<?php
	} else {
		if (isset($_GET['fehler'])) {
			//echo "<p><font size=6; color=#F0000;>" . $_GET['fehler'] . "</font></p>";
			echo '<div id="login_warning" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Fehler!</strong> ' . $_GET['fehler'] . '</div>';
		}
		?>
	<form action="login.php" method="post" name="regform">
		<div class="form-group">
		  <label for="usr">Name:</label>
		  <input type="text" class="form-control" id="usr" name="username">
		</div>
		<div class="form-group">
		  <label for="pwd">Password:</label>
		  <input type="password" class="form-control" id="pwd" name="password">
		</div>
		<button type="submit" class="btn btn-secondary btn-lg btn-block">Anmelden</button>
	</form>
	<?php
	}
	?>
</div>
<script>
	jQuery('#datetimepicker').datetimepicker({format:'Y-m-d H:i:s',inline:true, lang:'de', step: 5});
		
		function click_send_command(command) {
				if(command == "lock_forced"){
					$('#ask_confirm_locked').modal('hide');
				}
				if(command == "unlock_forced"){
					$('#ask_confirm_unlocked').modal('hide');
				}
				
				user_login = $("#user_login").html();
				
				$.ajax({
				url: "send_command_now.php?command="+command+"&user_online="+user_login+"&randval="+Math.random(),
				success: function (data_com) {
						//console.log(data_com);
						if (data_com == 'command_unknow') {
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Warnung! </strong> Befehl wurde nicht erkannt!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						} else if (data_com == 'error_other_command_running') {
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Warnung! </strong> Laufender Befehl erkannt bitte warten...</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						} else if (data_com == 'ok') {
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich!</strong> Befehl gesendet!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						} else if (data_com == 'db_error') {
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong> Fehler beim schreiben in die DB!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						} else {
							$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong> Syntax Error!</div>' + $("#active_alerts").html());
							active_alerts_array.push(active_alerts)
						}
					}
				});
		}
		
		function save_plan(){
			auto_lock = ""
			user_login = $("#user_login").html();
			date = $("#datetimepicker").val();
			
			if ($('#lock_after_close').is(":checked")){
				auto_lock = "lock_after_close"
			}
			
			$.post("send_command_job.php", {datetimepicker: date, lock_after_close: auto_lock, user_online: user_login},
			function (data) {
				if (data == 'datum_leer') {
					$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Warning! </strong> Datum darf nicht leer sein.</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
				} else if (data == 'datum_muss_zunkunft') {
					$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Warning! </strong> Datum muss in der Zukunft liegen</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
				} else if (data == 'ok') {
					//logs read give the feedback
					//$("#active_alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Erfolgreich!</strong> Geplantes aufsperren wurde gespeichert!</div>' + $("#active_alerts").html());
				} else if (data == 'error_db') {
					$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong> Fehler beim schreiben in die DB!</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
				} else {
					$("#active_alerts").html('<div id="active_alert_'+ (++active_alerts) +'" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong> Syntax Error! <br> '+ data +'</div>' + $("#active_alerts").html());
					active_alerts_array.push(active_alerts)
				}
			});
		}
</script>
</body>
</html>
