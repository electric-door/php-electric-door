<?php
require_once('db/database.php');

$command_array = '';
$c_i = 0;
	$sql1 = "SELECT logs_id, status, time FROM door_logs";
	$result1 = $conn->query($sql1);
	if ($result1->num_rows > 0) {
		while($row = $result1->fetch_assoc()) {
			$c_i++;
			$timestamp = time();
            $datum = date("Y-m-d", $timestamp);
			$year =  date("Y", $timestamp);
			$day = date("d", $timestamp);
			
			$m = date('m', strtotime($row["time"]));
            $d = date('d', strtotime($row["time"]));
            $Y = date('Y', strtotime($row["time"]));
			$s = date('s', strtotime($row["time"]));
            $i = date('i', strtotime($row["time"]));
            $H = date('H', strtotime($row["time"]));
			
			$command_datum = $Y."-".$m."-".$d;
			
			if($datum == $command_datum){
				$command_time_display = "Heute ".$H.":".$i.":".$s."";
			}else{
				if($year == $Y){
					if($d == ($day-1)){
						$command_time_display = "Gestern ".$H.":".$i.":".$s."";
					}else{
						$command_time_display = "".$d."-".$m." ".$H.":".$i.":".$s."";
					}
				}else{
					$command_time_display = "".$Y."-".$d."-".$m." ".$H.":".$i.":".$s."";
				}
				
			}
			
			if($row["status"] == "opened"){
				$command_display = "geöffnet";
			}else{
				$command_display = "geschlossen";
			}
			if($command_array != ''){
				$command_array.= ',';
			}
			$command_array.= '{"id":"'.$row["logs_id"].'","time":"'.$command_time_display.'","command":"'.$command_display.'"}';
			//$command_array.= '<tr><td>'.$row["logs_id"].'</td><td>'. $command_time_display.'</td><td>'.$command_display.'</td></tr>';
		}
	}
$command_array_header = '{"draw": 1,"recordsTotal": '.$c_i.', "recordsFiltered": '.$c_i.', "data":[';
$command_array_body = $command_array;
$command_array_footer =']}';

echo ($command_array_header."".$command_array_body."".$command_array_footer);

//echo json_encode($command_array_header."".$command_array_body."".$command_array_footer);