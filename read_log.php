<?php
require_once('db/database.php');
$command_array = '';
$ic = 0;
	$sql = "SELECT commad_id, type, command, execute_time, lock_after_close, feedback, feedback_time, command_time, user FROM commands";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$ic++;
			
			$timestamp = time();
            $datum = date("Y-m-d", $timestamp);
			$year =  date("Y", $timestamp);
			$day = date("d", $timestamp);
            $uhrzeit_aktue = date("H:i:s", $timestamp);

            $m = date('m', strtotime($row["command_time"]));
            $d = date('d', strtotime($row["command_time"]));
            $Y = date('Y', strtotime($row["command_time"]));
			$s = date('s', strtotime($row["command_time"]));
            $i = date('i', strtotime($row["command_time"]));
            $H = date('H', strtotime($row["command_time"]));
			
			$command_datum = $Y."-".$m."-".$d;
			
			if($datum == $command_datum){
				$command_time_display = "Heute ".$H.":".$i."";
			}else{
				if($year == $Y){
					if($d == ($day-1)){
						$command_time_display = "Gestern ".$H.":".$i."";
					}else{
						$command_time_display = "".$d."-".$m." ".$H.":".$i."";
					}
				}else{
					$command_time_display = "".$Y."-".$d."-".$m." ".$H.":".$i."";
				}
				
			}
			
			if($row["execute_time"] != 0){
				$m = date('m', $row["execute_time"]);
				$d = date('d', $row["execute_time"]);
				$Y = date('Y', $row["execute_time"]);
				$s = date('s', $row["execute_time"]);
				$i = date('i', $row["execute_time"]);
				$H = date('H', $row["execute_time"]+3600);
				
				$command_datum = $Y."-".$m."-".$d;
				
				if($datum == $command_datum){
					$execute_time_display = "Heute ".$H.":".$i."";
				}else{
					if($year == $Y){
						if($d == ($day-1)){
							$execute_time_display = "Gestern ".$H.":".$i."";
						}else{
							$execute_time_display = "".$d."-".$m." ".$H.":".$i."";
						}
					}else{
						$execute_time_display = "".$Y."-".$d."-".$m." ".$H.":".$i."";
					}
					
				}
			}
			
			switch ($row["command"]) {
				case "lock":
					$command_display = "Zusperren";
					break;
				case "unlock":
					$command_display = "Aufsperren";
					break;
				case "lock_forced":
					$command_display = "!ZUSPERREN";
					break;
				case "unlock_forced":
					$command_display = "!AUFSPERREN";
					break;
				default:
					$command_display = "unknow_command";
			}
			
			if($row["lock_after_close"] == "0"){
				$lock_after_close_display = "";
			}else{
				$lock_after_close_display = "(AUTO Zusperren)";
			}
						
			if($row["feedback"] != "0"){
				if($row["feedback"] == "ok_door_unlocked" || $row["feedback"] == "ok_door_locked"){
					$feedback_display = "Erledigt";
				}elseif($row["feedback"] == "error_already_locked"){
					$feedback_display = "Error: ist bereits zugesperrt";
				}elseif($row["feedback"] == "error_already_unlocked"){
					$feedback_display = "Error: ist bereits aufgesperrt";
				}elseif($row["feedback"] == "error_door_open"){
					$feedback_display = "Error: Türe Offen";
				}
				
				$check_diff = $row["feedback_time"] - $row["execute_time"];
				if($check_diff > 0 AND $check_diff < 20){
					$feedback_time_display = "(".$check_diff."s)";
			}else{
				$feedback_time_display = "(ERR)";
			}
			}else{
				$feedback_display = "warten";
				$feedback_time_display = "...";
			}
			
			if($command_array != ''){
				$command_array.= ',';
			}
			
			if($row["type"] == "now"){
				$command_array.= '{"id":"'.$row["commad_id"].'","command_time":"'.$row["command_time"].'","execute_time":"'.$row["execute_time"].'","time_display":"'.$command_time_display.'","user":"'.$row["user"].'","command_row":"'.$row["command"].'","command_type":"'.$row["type"].'","command_display":"'.$command_display.'","feedback":"'.$feedback_display .' '. $feedback_time_display.'","feedback_row":"'.$row["feedback"].'", "feedback_time":"'.$row["feedback_time"].'"}';
				//$command_array.= '<tr><td>'.$row["commad_id"].'</td><td>'. $command_time_display.'</td><td>'.$row["user"].'</td><td>'.$type_display.' '.$command_display.'</td><td>'.$feedback_display .' '. $feedback_time_display.'</td></tr>';
			}else{
				$command_array.= '{"id":"'.$row["commad_id"].'","command_time":"'.$row["command_time"].'","execute_time":"'.$row["execute_time"].'","time_display":"'.$command_time_display.'","user":"'.$row["user"].'","command_row":"'.$row["command"].'","command_type":"'.$row["type"].'","command_display":"'.$command_display.' '.$execute_time_display.' '.$lock_after_close_display .'","feedback":"'.$feedback_display .' '. $feedback_time_display.'","feedback_row":"'.$row["feedback"].'", "feedback_time":"'.$row["feedback_time"].'"}';
				//$command_array.= '<tr><td>'.$row["commad_id"].'</td><td>'. $command_time_display.'</td><td>'.$row["user"].'</td><td>'.$type_display.' '.$execute_time_display.' '.$command_display.' '.$lock_after_close_display .'</td><td>'.$feedback_display .' '. $feedback_time_display.'</td></tr>';
			}
		}
	}
$command_array_header = '{"draw": 1,"recordsTotal": '.$ic.', "recordsFiltered": '.$ic.', "data":[';
$command_array_body = $command_array;
$command_array_footer =']}';

echo ($command_array_header."".$command_array_body."".$command_array_footer);