<?php
	require_once('db/database.php');
	session_start();
	
	if (isset($_POST['password']) AND $_POST['password'] <> "") {
		$passwort2 = md5(mysqli_real_escape_string($conn, $_POST['password']));
	} else {
		$passwort2 = "";
	}
	
	if (isset($_POST['username']) AND $_POST['username'] <> "") {
		$username = strtolower(mysqli_real_escape_string($conn, $_POST['username']));
	} else {
		$username = "";
	}
	
	//echo $username;
	//echo $passwort2;
	
	$sql = "SELECT uid, name, password, rolle, active FROM user WHERE name = '".$username."'";
	$result = $conn->query($sql);
	if ($result->num_rows == 1 ) {
		while($row = $result->fetch_assoc()) {
			$active = $row["active"];
			$password = $row["password"];
			$user_rolle = $row["rolle"];
			
			if($active != 1){
				header("Location:index.php?fehler=User%20ist%20gesperrt%20oder%20nicht%20aktiv!");
				exit;
			}
			
			if($passwort2 != $password){
				header("Location:index.php?fehler=Falsches%20Passwort%20oder%20Benutzername!");
				exit;
			}else{
				$_SESSION['id'] = session_id();
				$_SESSION['name'] = $username;
				$_SESSION['roll'] = $user_rolle;
				
				//check login with admin and its have the default pw
				if($username == "admin" and $passwort2 == "21232f297a57a5a743894a0e4a801fc3"){
					$_SESSION['first_time'] = "true";
				}else{
					$_SESSION['first_time'] = "false";
				}
				
				setcookie('user', $username, time() + (86400 * 365), "/");
				setcookie('roll', $$user_rolle, time() + (86400 * 365), "/");
				
				header("Location:index.php");
				exit;
			}
			
		}
	}else{
		header("Location:index.php?fehler=Falsches%20Passwort%20oder%20Benutzername!");
		exit;
	}
	