<?php
require_once('db/database.php');

$command_array = '';
$c_i = 0;
	$sql1 = "SELECT uid, name, rolle, active FROM user";
	$result1 = $conn->query($sql1);
	if ($result1->num_rows > 0) {
		while($row = $result1->fetch_assoc()) {
			$c_i++;
			
			if($command_array != ''){
				$command_array.= ',';
			}
			
			$active = $row["active"];
			
			if($active == 1){
				$active_display = "Ja";
			}else{
				$active_display = "Nein";
			}
			
			$command_array.= '{"id":"'.$row["uid"].'","name":"'.$row["name"].'","rolle":"'.$row["rolle"].'","aktiv":"'.$active_display.'"}';
			//$command_array.= '<tr><td>'.$row["logs_id"].'</td><td>'. $command_time_display.'</td><td>'.$command_display.'</td></tr>';
		}
	}
$command_array_header = '{"draw": 1,"recordsTotal": '.$c_i.', "recordsFiltered": '.$c_i.', "data":[';
$command_array_body = $command_array;
$command_array_footer =']}';

echo ($command_array_header."".$command_array_body."".$command_array_footer);