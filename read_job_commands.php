<?php
require_once('db/database.php');

$command_array = '';
$c_i = 0;
	$sql = "SELECT commad_id, type, command, execute_time, lock_after_close, feedback, feedback_time, command_time, user FROM commands WHERE feedback_time=0 AND type='job'";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$c_i++;
			$timestamp = time();
            $datum = date("Y-m-d", $timestamp);
			$year =  date("Y", $timestamp);
			$day = date("d", $timestamp);
            $uhrzeit_aktue = date("H:i:s", $timestamp);

            $m = date('m', strtotime($row["command_time"]));
            $d = date('d', strtotime($row["command_time"]));
            $Y = date('Y', strtotime($row["command_time"]));
			$s = date('s', strtotime($row["command_time"]));
            $i = date('i', strtotime($row["command_time"]));
            $H = date('H', strtotime($row["command_time"]));
			
			$command_datum = $Y."-".$m."-".$d;
			
			if($datum == $command_datum){
				$command_time_display = "Heute ".$H.":".$i."";
			}else{
				if($year == $Y){
					if($d == ($day-1)){
						$command_time_display = "Gestern ".$H.":".$i."";
					}else{
						$command_time_display = "".$d."-".$m." ".$H.":".$i."";
					}
				}else{
					$command_time_display = "".$Y."-".$d."-".$m." ".$H.":".$i."";
				}
				
			}
			
				$type_display = "";
			
			if($row["execute_time"] != 0){
				$m = date('m', $row["execute_time"]);
				$d = date('d', $row["execute_time"]);
				$Y = date('Y', $row["execute_time"]);
				$s = date('s', $row["execute_time"]);
				$i = date('i', $row["execute_time"]);
				$H = date('H', $row["execute_time"]);
				
				$command_datum = $Y."-".$m."-".$d;
				
				if($datum == $command_datum){
					$execute_time_display = "Heute um ".$H.":".$i."";
				}else{
					if($year == $Y){
						if($d == ($day-1)){
							$execute_time_display = "Gestern um ".$H.":".$i."";
						}else{
							$execute_time_display = "".$d."-".$m." um ".$H.":".$i."";
						}
					}else{
						$execute_time_display = "".$Y."-".$d."-".$m." um ".$H.":".$i."";
					}
					
				}
			}
			
			switch ($row["command"]) {
				case "lock":
					$command_display = "zusperren";
					break;
				case "unlock":
					$command_display = "aufsperren";
					break;
				case "lock_forced":
					$command_display = "zusperren !FORCED!";
					break;
				case "unlock_forced":
					$command_display = "aufsperren !FORCED!";
					break;
				default:
					$command_display = "unknow_command";
			}
			
			if($row["lock_after_close"] == "0"){
				$lock_after_close_display = "Nein";
			}else{
				$lock_after_close_display = "Ja";
			}
			
			if($command_array != ''){
				$command_array.= ',';
			}
			
			$command_array.= '{"id":"'.$row["commad_id"].'","time":"'.$command_time_display.'","user":"'.$row["user"].'","command":"'.$type_display.' '.$execute_time_display.' '.$command_display.'","zusperren":"'.$lock_after_close_display .'"}';
			//$command_array.= '<tr><td>'.$row["commad_id"].'</td><td>'. $command_time_display.'</td><td>'.$row["user"].'</td><td>'.$type_display.' '.$execute_time_display.' '.$command_display.'</td><td>'.$lock_after_close_display .'</td></tr>';
		}
	}
	
$command_array_header = '{"draw": 1,"recordsTotal": '.$c_i.', "recordsFiltered": '.$c_i.', "data":[';
$command_array_body = $command_array;
$command_array_footer =']}';

echo ($command_array_header."".$command_array_body."".$command_array_footer);