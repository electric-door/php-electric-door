<?php
date_default_timezone_set('Europe/Berlin');
require_once('db/database.php');
session_start();

if (isset($_GET['js_check']) AND $_GET['js_check'] <> "") {
	$js_check = mysqli_real_escape_string($conn, $_GET['js_check']);
} else {
	$js_check=0;
}



$roll_found = 0;
$user_name = $_SESSION['name'];

$roll_logs = false;
$roll_user_controll = false;
$roll_planen = false;
$roll_foce_command = false;
$roll_unlock = false;
$roll_lock = false;

//if session roll are empty logout
if ($user_name == "") {
	header("Location:logout.php");
	exit;
}
function check_priv_valid($data_roll){
	$timestamp = time();
	$datum = date("Y-m-d", $timestamp);
	
	$tage = array("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa");
	$tag = date("w");
	$heute_wo = $tage[$tag];
	if($data_roll == "1"){
		return true;
	}elseif($data_roll == "0"){
		return false;
	}else{
	
	$check_time = false;
	$time_start = "";
	$time_end = "";
	//check herer date or time privs set
		$privs_special = explode('|', $data_roll);
		foreach ($privs_special as $privs_data){
			if (!is_numeric($privs_data)) {
				//tag
				if($heute_wo == $privs_data){
					$check_time = true;
				}
			}else{
				//uhrzeit
				if($time_start == ""){
					$time_start = $datum." ".$privs_data.":00:00";
				}else{
					$time_end = $datum." ".$privs_data.":00:00";
				}
			}
		}
	}
	
	if($time_start != "" and $time_end != ""){
		//check time are ok
		if (time() >= strtotime($time_start) and time() <= strtotime($time_end)) {
			if($check_time == true){
				return true;
			}
		}
	}else{
		if($check_time == true){
			return true;
		}
	}
}

$sql = "SELECT uid, rolle FROM user WHERE name='".$user_name."'";
	$result = $conn->query($sql);
	if ($result->num_rows == 1) {
		while($row = $result->fetch_assoc()) {
			$user_rolle = $row["rolle"];
		}
	}else{
		header("Location:logout.php");
		exit;
	}


$sql1 = "SELECT name, data FROM rolle";
$result1 = $conn->query($sql1);
if ($result1->num_rows > 0) {
	while($row = $result1->fetch_assoc()) {
		if($user_rolle == $row["name"]){
			$roll_found++;
			$roll_data = $row["data"];
			
			$data_array = explode(';', $roll_data);
			foreach ($data_array as $values)
			{
				$privs = explode(':', $values);
				$priv_name = $privs[0];
				$priv_value = $privs[1];
				
				//echo $priv_name." ".$priv_value."<br>";
				
				if($priv_name == "roll_foce_command"){
					$roll_foce_command = check_priv_valid($priv_value);
				}
				if($priv_name == "roll_unlock"){
					$roll_unlock = check_priv_valid($priv_value);
				}
				if($priv_name == "roll_lock"){
					$roll_lock = check_priv_valid($priv_value);
				}
				if($priv_name == "roll_planen"){
					$roll_planen = check_priv_valid($priv_value);
				}
				if($priv_name == "roll_logs"){
					$roll_logs = check_priv_valid($priv_value);
				}
				if($priv_name == "roll_user_controll"){
					$roll_user_controll = check_priv_valid($priv_value);
				}
			}
		}
	}
}

//if the roll dont found, logout
if($roll_found == 0){
	header("Location:logout.php");
	exit;
}else{
	if($js_check == 1){
		echo $roll_foce_command.",".$roll_unlock.",".$roll_lock.",".$roll_planen.",".$roll_logs.",".$roll_user_controll;
	}
}