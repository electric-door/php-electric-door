<?php
define("LOCALDB", false);

if (LOCALDB){
	$servername = "localhost:3306";
	$username = "xxxxxx"; //your db user for local
	$password = "xxxxx"; //your db user passwort for local
	$dbname = "door_project";
}else{
	$servername = "localhost";
	$username = "xxxxxxx"; //your db user
	$password = "xxxxxxx"; //your db user passwort
	$dbname = "door_project";
}
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}