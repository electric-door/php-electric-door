<?php
require_once('db/database.php');
	$sql = "SELECT oc_time, open_close, lu_time, lock_unlock FROM door_status";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$timestamp = time();
			$datum = date("Y-m-d", $timestamp);
			$year =  date("Y", $timestamp);
			$day = date("d", $timestamp);
			$uhrzeit_aktue = date("H:i:s", $timestamp);
			
			//open
			$db_open_close = $row["open_close"];
			$db_open_close_time = $row["oc_time"];
			
			$m = date('m', strtotime($row["oc_time"]));
			$d = date('d', strtotime($row["oc_time"]));
			$Y = date('Y', strtotime($row["oc_time"]));
			$s = date('s', strtotime($row["oc_time"]));
			$i = date('i', strtotime($row["oc_time"]));
			$H = date('H', strtotime($row["oc_time"]));
			
			$command_datum_oc = $Y."-".$m."-".$d;
					
			if($datum == $command_datum_oc){
				$command_time_display_oc = "{Heute ".$H.":".$i.":".$s."}";
			}else{
				if($year == $Y){
					if($d == ($day-1)){
						$command_time_display_oc = "{Gestern ".$H.":".$i.":".$s."}";
					}else{
						$command_time_display_oc = "{".$d."-".$m." ".$H.":".$i.":".$s."}";
					}
				}else{
					$command_time_display_oc = "{".$Y."-".$d."-".$m." ".$H.":".$i.":".$s."}";
				}
			}
			
			//locked
			$db_lock_unlock = $row["lock_unlock"];
			$db_lock_unlock_time = $row["lu_time"];
			
			$m = date('m', strtotime($row["lu_time"]));
			$d = date('d', strtotime($row["lu_time"]));
			$Y = date('Y', strtotime($row["lu_time"]));
			$s = date('s', strtotime($row["lu_time"]));
			$i = date('i', strtotime($row["lu_time"]));
			$H = date('H', strtotime($row["lu_time"]));
					
			$command_datum_lu = $Y."-".$m."-".$d;
					
			if($datum == $command_datum_lu){
				$command_time_display_lu = "{Heute ".$H.":".$i.":".$s."}";
			}else{
				if($year == $Y){
					if($d == ($day-1)){
						$command_time_display_lu = "{Gestern ".$H.":".$i.":".$s."}";
					}else{
						$command_time_display_lu = "{".$d."-".$m." ".$H.":".$i.":".$s."}";
					}
				}else{
					$command_time_display_lu = "{".$Y."-".$d."-".$m." ".$H.":".$i.":".$s."}";
				}
			}
		}
	}
	
$output = '{"door_status":[{"status":"'.$db_lock_unlock.'","time":"'.$db_lock_unlock_time.'","display_time":"'.$command_time_display_lu.'"}, {"status":"'.$db_open_close.'","time":"'.$db_open_close_time.'","display_time":"'.$command_time_display_oc.'"}]}';
echo $output;