<?php
require_once('db/database.php');

if (isset($_GET['option']) AND $_GET['option'] <> "") {
		$option = strtolower(mysqli_real_escape_string($conn, $_GET['option']));
	} else {
		$option = "data_table";
	}

$command_array_data_table = '';
$command_array_select = '';
$privs_roll_name = array("roll_foce_command"=> "Erzwungene Actionen","roll_unlock"=> "Aufsperren","roll_lock"=> "Zusperren","roll_planen"=> "Planen","roll_logs"=> "Logs","roll_user_controll"=> "User");

$c_i = 0;
	$sql1 = "SELECT rid, name, data FROM rolle";
	$result1 = $conn->query($sql1);
	if ($result1->num_rows > 0) {
		while($row = $result1->fetch_assoc()) {
			$roll_used = "";
			$c_i++;
			$command_data_array = '';
			
			if($command_array_data_table != ''){
				$command_array_data_table.= ', ';
			}
			
			if($command_array_select != ''){
				$command_array_select.= ',';
			}
			
			$data_array = explode(';', $row["data"]);
			foreach ($data_array as $values)
			{
				$privs = explode(':', $values);
				$priv_name = $privs[0];
				$priv_value = $privs[1];
				$value_name = "";
				
				if($priv_value == "1"){
				$value_name = "Ja {Immer}";
				}elseif($priv_value == "0"){
					$value_name = "Nein";
				}else{
					$value_name.="Ja { ";
					$time_start = "";
					$time_end = "";
					$privs_special = explode('|', $priv_value);
					foreach ($privs_special as $privs_data){
						if (!is_numeric($privs_data)) {
							//tag
							$value_name.= $privs_data.", ";
						}else{
							//uhrzeit
							if($time_start == ""){
								$time_start = $privs_data.":00";
								$value_name.= "Von ". $time_start." ";
							}else{
								$time_end = $privs_data.":00";
								$value_name.= "Bis ". $time_end;
							}
						}
					}
					$value_name.=" }";
				}
				
				$command_data_array.= "[".$privs_roll_name[$priv_name].": ".$value_name."] <br>";
			}
			
			$sql2 = "SELECT uid, name FROM user WHERE rolle='".$row["name"]."'";
			$result2 = $conn->query($sql2);
			if ($result2->num_rows > 0) {
				while($row2 = $result2->fetch_assoc()) {
					$roll_used.= $row2["name"]."<br>";
				}
			}else{
				$roll_used = "Nein";
			}
			
			
			$command_array_data_table.= '{"id":"'.$row["rid"].'","name":"'.$row["name"].'","daten":"'.$command_data_array.'", "used":"'.$roll_used.'"}';
			$command_array_select.= $row["name"];
			
			
		}
	}
if($option == "data_table"){
	$command_array_header = '{"draw": 1,"recordsTotal": '.$c_i.', "recordsFiltered": '.$c_i.', "data":[';
	$command_array_body = $command_array_data_table;
	$command_array_footer =']}';

	echo ($command_array_header."".$command_array_body."".$command_array_footer);
}elseif($option == "select"){
	echo $command_array_select;
}
