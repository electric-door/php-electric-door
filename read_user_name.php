<?php
require_once('db/database.php');

if (isset($_GET['user_name']) AND $_GET['user_name'] <> "") {
		$user_name = strtolower(mysqli_real_escape_string($conn, $_GET['user_name']));
	} else {
		$user_name = "empty";
	}

if($user_name != "empty"){
	$sql1 = "SELECT name FROM user";
	$result1 = $conn->query($sql1);
	if ($result1->num_rows > 0) {
		while($row = $result1->fetch_assoc()) {
			if ($user_name == $row["name"]){
				die("Error");
			}
		}
	}
}