<?php
session_start();
$_SESSION = array();
session_destroy();

//delet the cookie
$_SESSION['id'] = session_id();
$_SESSION['name'] = "";
setcookie('user', "", time() - 3600, "/");

header("Location:index.php");
exit;