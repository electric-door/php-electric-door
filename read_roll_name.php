<?php
require_once('db/database.php');

if (isset($_GET['roll_name']) AND $_GET['roll_name'] <> "") {
		$roll_name = strtolower(mysqli_real_escape_string($conn, $_GET['roll_name']));
	} else {
		$roll_name = "empty";
	}

if($roll_name != "empty"){
	$sql1 = "SELECT name FROM rolle";
	$result1 = $conn->query($sql1);
	if ($result1->num_rows > 0) {
		while($row = $result1->fetch_assoc()) {
			if ($roll_name == $row["name"]){
				die("Error");
			}
		}
	}
}