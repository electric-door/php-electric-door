-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 24. Mai 2021 um 04:16
-- Server-Version: 5.5.57-MariaDB
-- PHP-Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `door_project`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `commands`
--

CREATE TABLE `commands` (
  `commad_id` bigint(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  `command` varchar(30) NOT NULL,
  `execute_time` bigint(30) NOT NULL DEFAULT '0',
  `lock_after_close` varchar(100) NOT NULL DEFAULT '0',
  `feedback` varchar(100) NOT NULL DEFAULT '0',
  `feedback_time` int(20) NOT NULL DEFAULT '0',
  `user` varchar(100) NOT NULL,
  `command_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `door_logs`
--

CREATE TABLE `door_logs` (
  `logs_id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `door_status`
--

CREATE TABLE `door_status` (
  `status_id` int(11) NOT NULL,
  `lock_unlock` varchar(10) NOT NULL,
  `lu_time` datetime NOT NULL,
  `open_close` varchar(10) NOT NULL,
  `oc_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `door_status`
--

INSERT INTO `door_status` (`status_id`, `lock_unlock`, `lu_time`, `open_close`, `oc_time`) VALUES
(1, 'unlocked', '2021-05-15 19:42:51', 'closed', '2021-05-15 16:49:44');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `live`
--

CREATE TABLE `live` (
  `live_id` int(2) NOT NULL,
  `live_status` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `live`
--

INSERT INTO `live` (`live_id`, `live_status`) VALUES
(1, 1621097024);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rolle`
--

CREATE TABLE `rolle` (
  `rid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `data` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rolle`
--

INSERT INTO `rolle` (`rid`, `name`, `data`) VALUES
(1, 'admin', 'roll_unlock:1;roll_lock:1;roll_foce_command:1;roll_planen:1;roll_logs:1;roll_user_controll:1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `rolle` varchar(100) NOT NULL,
  `active` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`uid`, `name`, `password`, `rolle`, `active`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `commands`
--
ALTER TABLE `commands`
  ADD PRIMARY KEY (`commad_id`);

--
-- Indizes für die Tabelle `door_logs`
--
ALTER TABLE `door_logs`
  ADD PRIMARY KEY (`logs_id`);

--
-- Indizes für die Tabelle `door_status`
--
ALTER TABLE `door_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indizes für die Tabelle `live`
--
ALTER TABLE `live`
  ADD PRIMARY KEY (`live_id`);

--
-- Indizes für die Tabelle `rolle`
--
ALTER TABLE `rolle`
  ADD PRIMARY KEY (`rid`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `commands`
--
ALTER TABLE `commands`
  MODIFY `commad_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `door_logs`
--
ALTER TABLE `door_logs`
  MODIFY `logs_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `door_status`
--
ALTER TABLE `door_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `live`
--
ALTER TABLE `live`
  MODIFY `live_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `rolle`
--
ALTER TABLE `rolle`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
